import React, { PureComponent } from 'react';
import { TouchableOpacity, Text, View } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import { Icon } from 'native-base';

export default class MenuButton extends PureComponent {
  render() {
    return (
      <TouchableOpacity
        onPress={this.props.onPress}
        style={styles.btnClickContain}
      >
        <View style={styles.btnContainer}>
          <Icon name={this.props.source} type={this.props.type} style={{ fontSize: 25, width: 30 }} />
          <Text style={styles.btnText}>{this.props.title}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

MenuButton.propTypes = {
  onPress: PropTypes.func,
  source: PropTypes.string,
  title: PropTypes.string,
  type: PropTypes.string,
};
