import React, { PureComponent } from 'react';
import { Image } from 'react-native';
import * as FileSystem from 'expo-file-system'
import shorthash from 'shorthash';
import PropTypes from 'prop-types'


export default class CacheImage extends PureComponent {

    state = { source: { uri: null } };

    render() {
        return (
            <Image
                source={this.state.source}
                style={this.props.style}
            />
        );
    }

    componentWillReceiveProps = async (nextProps) => {
        if (nextProps.source !== this.props.source) {
            const { source } = nextProps;
            if (source.uri) {
                const name = shorthash.unique(source.uri);
                const path = `${FileSystem.cacheDirectory}${name}`;
                const image = await FileSystem.getInfoAsync(path);
                if (image.exists) {
                    this.setState({ source: { uri: image.uri } });
                    return;
                }

                const newImage = await FileSystem.downloadAsync(source.uri, path);
                this.setState({ source: { uri: newImage.uri } });
            }
        }
    };
}

CacheImage.propTypes = {
    style: PropTypes.object,
    source: PropTypes.object
}

