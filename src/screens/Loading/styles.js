import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
        backgroundColor: '#6781a4',
        width: '100%',
        height: '100%'
    },
    indicator: {
        justifyContent: 'center',

    }
});

export default styles;
