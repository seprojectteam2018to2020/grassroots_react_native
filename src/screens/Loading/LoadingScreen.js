import React, { PureComponent } from 'react';
import { ActivityIndicator, AsyncStorage, StatusBar, View, Platform } from 'react-native';
import styles from './styles';
import * as Font from 'expo-font';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';

export default class LoadingScreen extends PureComponent {

    constructor(props) {
        super(props);
        this._bootstrapAsync();
        state = {
            notification: {},
        };
    }


    _bootstrapAsync = async () => {
        const expo_token = await Notifications.getExpoPushTokenAsync();
        if (this.props.navigation.getParam('type') === ENV.loadType.LOGOUT) {
            Axios.post(API_HOST_NAME + '/modify-token-user', {
                expo_token: expo_token,
                type: 0
            })

            delete Axios.defaults.headers.common['Authorization'];
            await AsyncStorage.removeItem('apiToken');
            this.props.navigation.navigate('Auth');

        } else {
            const token = await AsyncStorage.getItem('apiToken');
            if (token) {
                Axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;

                if (!this.props.navigation.getParam('type')) {
                    Axios.post(API_HOST_NAME + '/token', {
                        expo_token: expo_token
                    })
                }
            }

            this._loadFont().then(() => {
                this.props.navigation.navigate(token ? 'App' : 'Auth');
            });
        }

    };

    _loadFont = async () => {
        if (Platform.OS === 'android') {
            await Font.loadAsync({
                'Roboto_medium': require('../../../node_modules/native-base/Fonts/Roboto_medium.ttf'),
                // icon font
                'MaterialIcons': require('../../../node_modules/native-base/Fonts/MaterialIcons.ttf'),
                'AntDesign': require('../../../node_modules/native-base/Fonts/AntDesign.ttf'),
                'Foundation': require('../../../node_modules/native-base/Fonts/Foundation.ttf'),
                'MaterialCommunityIcons': require('../../../node_modules/native-base/Fonts/MaterialCommunityIcons.ttf'),

            });
        } else {
            await Font.loadAsync({
                'Material Icons': require('../../../node_modules/native-base/Fonts/MaterialIcons.ttf'),
                'anticon': require('../../../node_modules/native-base/Fonts/AntDesign.ttf'),
                'fontcustom': require('../../../node_modules/native-base/Fonts/Foundation.ttf'),
                'Material Design Icons': require('../../../node_modules/native-base/Fonts/MaterialCommunityIcons.ttf'),
            });
        }

        await Font.loadAsync({
            'FontAwesome': require('../../../node_modules/native-base/Fonts/FontAwesome.ttf'),
            'Entypo': require('../../../node_modules/native-base/Fonts/Entypo.ttf'),
            'Octicons': require('../../../node_modules/native-base/Fonts/Octicons.ttf'),
            'Ionicons': require('../../../node_modules/native-base/Fonts/Ionicons.ttf'),
        });


    }

    registerForPushNotificationsAsync = async () => {
        const { status: existingStatus } = await Permissions.getAsync(
            Permissions.NOTIFICATIONS
        );
        let finalStatus = existingStatus;

        // only ask if permissions have not already been determined, because
        // iOS won't necessarily prompt the user a second time.
        if (existingStatus !== 'granted') {
            // Android remote notification permissions are granted during the app
            // install, so this will only ask on iOS
            const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
            finalStatus = status;
        }

        // Stop here if the user did not grant permissions
        if (finalStatus !== 'granted') {
            await AsyncStorage.setItem('notification', 'false');
            return;
        }

        const token = await Notifications.getExpoPushTokenAsync();
        Axios.post(API_HOST_NAME + '/token', {
            expo_token: token
        })
    }

    componentWillMount() {
        this.registerForPushNotificationsAsync();
    }

    render() {
        return (
            <View style={styles.container}>
                <ActivityIndicator style={styles.indicator} size="large" color="#ff9000" />
                <StatusBar barStyle="default" />
            </View>
        );
    }


}