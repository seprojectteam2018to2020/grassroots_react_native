import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  listHeader: {
    justifyContent: 'center',
    backgroundColor: '#FFBA64'
  },
  gridView: {
    width: '47%',
    height: '47%',
    backgroundColor: '#FFC67D',
    borderRadius: 5,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },
  gridHeader: {
    marginTop: 10,
    paddingBottom: 6,
    paddingTop: 6,
    backgroundColor: '#FFFFFF',
    opacity: 0.7,
    color: '#0090FF',
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
  },
  gridIcon: {
    opacity: 0.7,
    marginTop: 5,
    color: '#0090FF',
    fontSize: 70,
    alignSelf: 'center',
  },

  slideView: {
    margin: 14,
    flex: 1,
    borderColor: '#FFBA64',
    borderWidth: 4,
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
  },
});

export default styles;
