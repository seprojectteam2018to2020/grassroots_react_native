import React, { PureComponent } from 'react';
import { ScrollView, TouchableHighlight, RefreshControl, AsyncStorage } from 'react-native';
import { Container, Icon, View, ListItem, Text, Left, Body, Right, Button, Tab, Tabs } from 'native-base';
import styles from './styles';
import Carousel from 'react-native-snap-carousel';
import styled from "styled-components/native";
import { Placeholder, PlaceholderLine, ShineOverlay } from 'rn-placeholder';
import CacheImage from '../../components/CacheImage';
import DropdownAlert from 'react-native-dropdownalert';

export default class HomeScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.props = props;
    this._carousel = {};
    this.init();
  }

  init() {
    this.state = {
      eventArray: [],
      notificationArray: [],
      refreshing: false,
      isReady: false,
    }
  }

  render = () => {
    let { navigation } = this.props;

    let news = this.state.notificationArray.filter(notif => notif.type == '1').map((value, index) => {
      return <ListItem thumbnail key={value.id} onPress={() => {
        this.props.navigation.navigate('Notification', {
          id: value.id
        })
      }}>
        <Left>
          <Icon
            style={{ color: "red" }}
            name="new"
            type="Entypo"
          />
        </Left>
        <Body>
          <Text>{value.title}</Text>
          <Text note numberOfLines={1}>{value.sub_heading}</Text>
        </Body>
        <Right>
          <Button transparent>
            <Text>{tran.t('view')}</Text>
          </Button>
        </Right>
      </ListItem>
    })

    let information = this.state.notificationArray.filter(notif => notif.type == '2').map((value, index) => {
      return <ListItem thumbnail key={value.id} onPress={() => {
        this.props.navigation.navigate('Notification', {
          id: value.id
        })
      }}>
        <Left>
          <Icon
            style={{ color: color.blue }}
            name="like1"
            type="AntDesign"
          />
        </Left>
        <Body>
          <Text>{value.title}</Text>
          <Text note numberOfLines={1}>{value.sub_heading}</Text>
        </Body>
        <Right>
          <Button transparent>
            <Text>{tran.t('view')}</Text>
          </Button>
        </Right>
      </ListItem>
    })

    return (
      <Container>
        <ScrollView>
          <CarouselBackgroundView style={{ height: ENV.window.height * 0.32 }}>

            {(this.state.isReady) ?
              <Carousel
                ref={(c) => { this._carousel = c; }}
                data={this.state.eventArray}
                renderItem={this._renderItem.bind(this)}
                sliderWidth={400}
                itemWidth={300}
                layout={'default'}
                firstItem={1}
                loop={true}
                autoplay={true}
                autoplayInterval={10000}
              />
              :
              <Placeholder Animation={ShineOverlay} style={{ padding: 15 }} >
                <PlaceholderLine height={ENV.window.height * 0.29} />
              </Placeholder>}
          </CarouselBackgroundView>

          <View style={{ margin: 14, height: 270, flex: 1, flexDirection: 'row', alignContent: 'space-around', flexWrap: 'wrap', justifyContent: 'space-around' }}>
            <TouchableHighlight underlayColor={color.secondOrange}
              style={styles.gridView} onPress={() => { navigation.navigate('PointRecord') }}>
              <View>
                <Icon name="money" type="FontAwesome" style={styles.gridIcon} />
                <Text style={styles.gridHeader}>{tran.t('box_1')}</Text>
              </View>
            </TouchableHighlight>
            <TouchableHighlight underlayColor={color.secondOrange}
              style={styles.gridView} onPress={() => { navigation.navigate('EventRecord') }}>
              <View>
                <Icon name="calendar" type="FontAwesome" style={styles.gridIcon} />
                <Text style={styles.gridHeader}>{tran.t('box_2')}</Text>
              </View>
            </TouchableHighlight>
            <TouchableHighlight underlayColor={color.secondOrange}
              style={styles.gridView} onPress={() => {
                if (isAdmin) {
                  this.alert('warn', 'Warning', 'This Function is not acceptable foy you as you are administrator'); return;
                }
                navigation.navigate('CreateExpense')
              }}>
              <View>
                <Icon name="edit" type="FontAwesome" style={styles.gridIcon} />
                <Text style={styles.gridHeader}>{tran.t('box_3')}</Text>
              </View>
            </TouchableHighlight>
            <TouchableHighlight underlayColor={color.secondOrange}
              style={styles.gridView} onPress={() => {
                if (isAdmin) {
                  this.alert('warn', 'Warning', 'This Function is not acceptable foy you as you are administrator'); return;
                }
                navigation.navigate('Claim')
              }}>
              <View>
                <Icon name="list-alt" type="FontAwesome" style={styles.gridIcon} />
                <Text style={styles.gridHeader}>{tran.t('box_4')}</Text>
              </View>
            </TouchableHighlight>
          </View>

          <View style={styles.slideView}>
            <Tabs activeTextStyle={{ color: "#69a0ff" }}
              tabBarUnderlineStyle={{ backgroundColor: "#69a0ff", borderRadius: 5 }}>
              <Tab heading={tran.t('new')}
                activeTextStyle={{ color: '#0090FF' }}
                textStyle={{ color: '#696969' }}
                tabStyle={{ backgroundColor: color.secondOrange }}
                activeTabStyle={{ backgroundColor: color.secondOrange }}>
                <ScrollView style={{ height: ENV.window.height * 0.45 }}
                  nestedScrollEnabled={true}
                  refreshControl={
                    <RefreshControl
                      progressBackgroundColor={color.secondOrange}
                      tintColor={color.secondOrange}
                      refreshing={this.state.refreshing}
                      onRefresh={this._onRefresh}
                    />
                  } >
                  {news}
                </ScrollView>
              </Tab>
              <Tab heading={tran.t('information')} tabStyle={{ backgroundColor: color.secondOrange }}
                activeTabStyle={{ backgroundColor: color.secondOrange }}
                activeTextStyle={{ color: '#0090FF' }}
                textStyle={{ color: '#696969' }}>
                <ScrollView style={{ height: ENV.window.height * 0.45 }}
                  refreshControl={
                    <RefreshControl
                      progressBackgroundColor={color.secondOrange}
                      tintColor={color.secondOrange}
                      refreshing={this.state.refreshing}
                      onRefresh={this._onRefresh}
                    />
                  }>
                  {information}
                </ScrollView>
              </Tab>
            </Tabs>
          </View>
        </ScrollView>
        <DropdownAlert ref={ref => this.dropDownAlertRef = ref} />
      </Container >
    )
  }

  componentWillMount() {

  }

  componentDidMount() {
    this.getData()
  }

  pressCarouse(id) {
    this.props.navigation.navigate('Event', { id: id });
  }

  _renderItem = ({ item, index }) => {
    return (
      <ThumbnailBackgroundView>
        <CarouselSlider
          style={{ width: ENV.window.width * 0.77, }}
          onPress={() => {
            this.pressCarouse(item.id);
          }}
        >
          <CacheImage style={{
            height: ENV.window.height * 0.28, width: ENV.window.width * 0.77,
            top: 5, borderRadius: 10
          }} source={{ uri: item.image }} />
        </CarouselSlider>
      </ThumbnailBackgroundView>
    );
  }

  alert = (a, b, c) => this.dropDownAlertRef.alertWithType(a, b, c)

  getData = async () => {
    const notificationCache = await AsyncStorage.getItem('notificationCache');
    const eventCache = await AsyncStorage.getItem('eventCache');
    if (notificationCache && eventCache) {
      this.setState({
        notificationArray: JSON.parse(notificationCache),
        eventArray: JSON.parse(eventCache),
        isReady: true
      });

      Axios.all([
        Axios.get(API_HOST_NAME + '/notification'),
        Axios.get(API_HOST_NAME + '/event')
      ])
        .then(Axios.spread(async (responseA, responseB) => {
          if (responseA.status === 200 && responseB.status === 200) {

            if (JSON.stringify(responseA.data.data) != notificationCache) {
              this.setState({
                notificationArray: responseA.data.data
              })
              await AsyncStorage.setItem('notificationCache', JSON.stringify(responseA.data.data));

            }

            if (JSON.stringify(responseB.data.data.data) != eventCache) {
              this.setState({
                eventArray: responseB.data.data.data
              })
              await AsyncStorage.setItem('eventCache', JSON.stringify(responseB.data.data.data));
            }

          } else {
            this.alert('error', tran.t('network_error'), 'Cannot update the data');
          }

        }))
        .catch((error) => {
          this.alert('error', tran.t('unexpected_error'), error.message);
        });

    } else {
      Axios.all([
        Axios.get(API_HOST_NAME + '/notification'),
        Axios.get(API_HOST_NAME + '/event')
      ])
        .then(Axios.spread(async (responseA, responseB) => {

          if (responseA.status === 200 && responseB.status === 200) {
            this.setState({
              notificationArray: responseA.data.data,
              eventArray: responseB.data.data.data,
              isReady: true
            })

            await AsyncStorage.setItem('notificationCache', JSON.stringify(responseA.data.data));
            await AsyncStorage.setItem('eventCache', JSON.stringify(responseB.data.data.data));
          } else {
            this.alert('error', tran.t('network_error'), response.data.message);
          }

        }))
        .catch((error) => {
          this.alert('error', tran.t('unexpected_error'), error.message);
        });
    }
  }

  _onRefresh = () => {
    this.setState({ refreshing: true });
    this.getData().then(() => {
      setTimeout(() => { this.setState({ refreshing: false }) }, 1000);
    });
  }

}

const ThumbnailBackgroundView = styled.View`
  justify-content: center;
  align-items: center; 
`;

const CarouselSlider = styled.TouchableOpacity`
  margin-top: 8px;
  margin-right: 20px;
`
const CarouselBackgroundView = styled.View`
  background-color: #ffefd5;
  width: 100%;
`;

