import React, { PureComponent } from 'react';
import { ScrollView, RefreshControl } from 'react-native';
import { Container, Icon, View, ListItem, Text, Left, Body, Right, Button, Tab, Tabs } from 'native-base';
import styles from './styles';
import Carousel from 'react-native-snap-carousel';
import styled from "styled-components/native";

export default class HomeScreen extends PureComponent {
    constructor(props) {
        super(props);
        this.props = props;
        this._carousel = {};
        this.init();
    }

    init() {
        this.state = {
            eventArray: [],
            notificationArray: [],
        }
    }

    render = () => {


        let { navigation } = this.props;
        let news = this.state.notificationArray.filter(notif => notif.type == '1').map((value, index) => {
            return <ListItem thumbnail key={index} onPress={() => { navigation.navigate('Login') }}>
                <Left>
                    <Icon
                        style={{ color: "red" }}
                        name="new"
                        type="Entypo"
                    />
                </Left>
                <Body>
                    <Text>{value.title}</Text>
                    <Text note numberOfLines={1}>{value.sub_heading}</Text>
                </Body>
                <Right>
                    <Button transparent>
                        <Text>{tran.t('view')}</Text>
                    </Button>
                </Right>
            </ListItem>
        })

        let information = this.state.notificationArray.filter(notif => notif.type == '2').map((value, index) => {
            return <ListItem thumbnail key={index} onPress={() => { navigation.navigate('Login') }}>
                <Left>
                    <Icon
                        style={{ color: color.blue }}
                        name="like1"
                        type="AntDesign"
                    />
                </Left>
                <Body>
                    <Text>{value.title}</Text>
                    <Text note numberOfLines={1}>{value.sub_heading}</Text>
                </Body>
                <Right>
                    <Button transparent>
                        <Text>{tran.t('view')}</Text>
                    </Button>
                </Right>
            </ListItem>
        })

        return (
            <Container style={styles.container}>
                <ScrollView>
                    <CarouselBackgroundView >
                        <Carousel
                            ref={(c) => { this._carousel = c; }}
                            data={this.state.eventArray}
                            renderItem={this._renderItem.bind(this)}
                            sliderWidth={400}
                            itemWidth={300}
                            layout={'default'}
                            firstItem={1}
                            loop={true}
                            autoplay={true}
                            autoplayInterval={10000}
                        />
                    </CarouselBackgroundView>

                    <View style={styles.slideView}>
                        <Tabs activeTextStyle={{ color: "#69a0ff" }}
                            tabBarUnderlineStyle={{ backgroundColor: "#69a0ff", borderRadius: 5 }}>
                            <Tab heading={tran.t('new')}
                                tabStyle={{ backgroundColor: color.secondOrange }}
                                activeTabStyle={{ backgroundColor: color.secondOrange }}
                                activeTextStyle={{ color: '#0090FF' }}
                                textStyle={{ color: '#696969' }}>
                                <ScrollView style={{ height: ENV.window.height * 0.45 }}
                                    nestedScrollEnabled={true}
                                    refreshControl={
                                        <RefreshControl
                                            progressBackgroundColor={color.secondOrange}
                                            tintColor={color.secondOrange}
                                            refreshing={this.state.refreshing}
                                            onRefresh={this._onRefresh}
                                        />
                                    } >
                                    {news}
                                </ScrollView>
                            </Tab>
                            <Tab heading={tran.t('information')} tabStyle={{ backgroundColor: color.secondOrange }}
                                activeTabStyle={{ backgroundColor: color.secondOrange }}
                                activeTextStyle={{ color: '#0090FF' }}
                                textStyle={{ color: '#696969' }}
                            >
                                <ScrollView style={{ height: ENV.window.height * 0.45 }}
                                    refreshControl={
                                        <RefreshControl
                                            progressBackgroundColor={color.secondOrange}
                                            tintColor={color.secondOrange}
                                            refreshing={this.state.refreshing}
                                            onRefresh={this._onRefresh}
                                        />
                                    }>
                                    {information}
                                </ScrollView>
                            </Tab>
                        </Tabs>
                    </View>
                </ScrollView>
            </Container >
        );
    }

    componentWillMount() {

    }

    componentDidMount() {
        this.getData()
    }

    getData() {
        Axios.get(API_HOST_NAME + '/unlogin/notification')
            .then((response) => { this.setState({ notificationArray: response.data.data }) })
        // .catch((error) => {
        //     alert(error);
        // });

        Axios.get(API_HOST_NAME + '/unlogin/event')
            .then((response) => { this.setState({ eventArray: response.data.data.data }) })
        // .catch((error) => {
        //     alert(error);
        // });

    }

    pressCarouse(id) {
        this.props.navigation.navigate('Login');
    }

    _renderItem = ({ item, index }) => {
        return (
            <ThumbnailBackgroundView>
                <CarouselSlider
                    onPress={() => {
                        this.pressCarouse(item.id);
                    }}
                >
                    <CurrentSlide source={{ uri: item.image }} />
                </CarouselSlider>
            </ThumbnailBackgroundView>
        );
    }

}



const EventTitle = styled.Text`
  color: black;
  top: 5;
  justify-content: center;
`;
const CurrentSlide = styled.Image`
  top: 5;
  box-shadow: 5px 10px;
  width: 300;
  height: 194;                            
  border-radius: 10;
`;
// height: 184;

const ThumbnailBackgroundView = styled.View`
  justify-content: center;
  align-items: center; 
  width: 256;   
`;

const CarouselSlider = styled.TouchableOpacity`
`
const CarouselBackgroundView = styled.View`
  background-color: white;
  height: 210;
  width: 100%;
`;

