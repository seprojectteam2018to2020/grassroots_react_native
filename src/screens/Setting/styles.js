import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    scroll: {
        backgroundColor: 'white',
    },
    userRow: {
        alignItems: 'center',
        flexDirection: 'row',
        paddingBottom: 8,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 6,
    },
    userImage: {
        marginRight: 12,
    },
    listItemContainer: {
        height: 55,
        borderWidth: 0.5,
        borderColor: '#ECECEC',
    },
    htmlStyles: {
        fontSize: 18
    },
    headerIcon: {
        width: "60%",
        height: 80,
        marginTop: 50,
        marginBottom: 30,
        alignSelf: 'center',
    },
    confirmButton: {
        width: '90%',
        backgroundColor: '#ff9000',
        borderRadius: 5,
        height: 45,
        marginTop: 20,
        alignSelf: 'center'
    },
});

export default styles;
