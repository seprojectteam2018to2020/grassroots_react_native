import React from 'react'
import { Icon } from 'react-native-elements'

const Chevron = () => (
  <Icon
    name="chevron-right"
    type="Entypo"
    containerStyle={{ marginLeft: -15, width: 20 }}
  />
)

export default Chevron
