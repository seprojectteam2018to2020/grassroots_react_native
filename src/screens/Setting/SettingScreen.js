import React, { Component } from 'react'
import { ScrollView, Switch, View, AsyncStorage, Alert } from 'react-native'
import { ListItem } from 'react-native-elements'
import styles from "./styles";
import BaseIcon from '../../components/BaseIcon/Icon'
import Chevron from './Chevron'
import InfoText from './InfoText'
import { Text } from 'native-base';
import { Notifications } from 'expo';
import * as FileSystem from 'expo-file-system'
import Constants from 'expo-constants'
import DropdownAlert from 'react-native-dropdownalert';

export default class SettingScreen extends Component {
  constructor(props) {
    super(props);
    this.init();
  }

  init() {
    this.state = {
      pushNotifications: true,
      currency: '',
      location: '',
      language: '',
    }
  }

  render() {
    return (
      <ScrollView style={styles.scroll}>

        <InfoText text="Account" />
        <View>
          <ListItem
            hideChevron
            title={tran.t('s_push')}
            containerStyle={styles.listItemContainer}
            rightElement={
              <Switch
                onValueChange={this.onChangePushNotifications}
                value={this.state.pushNotifications}

              />
            }
            leftIcon={
              <BaseIcon
                containerStyle={{
                  backgroundColor: '#FFADF2',
                }}
                icon={{
                  type: 'material',
                  name: 'notifications',
                }}
              />
            }
          />
          <ListItem
            // chevron
            title={tran.t('s_currency')}
            rightTitle={tran.t(this.state.currency)}
            rightTitleStyle={{ fontSize: 15 }}
            onPress={() => this.triggerPicker(ENV.pickerType.currency)}
            containerStyle={styles.listItemContainer}
            leftIcon={
              <BaseIcon
                containerStyle={{ backgroundColor: '#FAD291' }}
                icon={{
                  type: 'font-awesome',
                  name: 'money',
                }}
              />
            }
            rightIcon={<Chevron />}
          />
          <ListItem
            title={tran.t('s_location')}
            rightTitle={tran.t(this.state.location)}
            rightTitleStyle={{ fontSize: 14 }}
            onPress={() => this.triggerPicker(ENV.pickerType.location)}
            containerStyle={styles.listItemContainer}
            leftIcon={
              <BaseIcon
                containerStyle={{ backgroundColor: '#57DCE7' }}
                icon={{
                  type: 'material',
                  name: 'place',
                }}
              />
            }
            rightIcon={<Chevron />}
          />
          <ListItem
            title={tran.t('s_language')}
            rightTitle={tran.t(tran.locale)}
            rightTitleStyle={{ fontSize: 15 }}
            onPress={() => this.triggerPicker(ENV.pickerType.language)}
            containerStyle={styles.listItemContainer}
            leftIcon={
              <BaseIcon
                containerStyle={{ backgroundColor: '#FEA8A1' }}
                icon={{
                  type: 'material',
                  name: 'language',
                }}
              />
            }
            rightIcon={<Chevron />}
          />
        </View>
        <InfoText text="More" />
        <View>
          <ListItem
            title={tran.t('s_aboutAs')}
            onPress={() => this.props.navigation.navigate('ContactUs')}
            containerStyle={styles.listItemContainer}
            leftIcon={
              <BaseIcon
                containerStyle={{ backgroundColor: '#A4C8F0' }}
                icon={{
                  type: 'ionicon',
                  name: 'md-information-circle',
                }}
              />
            }
            rightIcon={<Chevron />}
          />
          <ListItem
            title={tran.t('s_terms')}
            containerStyle={styles.listItemContainer}
            onPress={() => this.props.navigation.navigate('Terms')}
            leftIcon={
              <BaseIcon
                containerStyle={{ backgroundColor: '#C6C7C6' }}
                icon={{
                  type: 'entypo',
                  name: 'light-bulb',
                }}
              />
            }
            rightIcon={<Chevron />}
          />

          <ListItem
            title={tran.t('s_feedback')}
            containerStyle={styles.listItemContainer}
            onPress={() => this.props.navigation.navigate('Feedback')}
            leftIcon={
              <BaseIcon
                containerStyle={{
                  backgroundColor: '#4CD964',
                }}
                icon={{
                  type: 'materialicon',
                  name: 'feedback',
                }}
              />
            }
            rightIcon={<Chevron />}
          />
          <ListItem
            title={tran.t('s_cache')}
            containerStyle={styles.listItemContainer}
            onPress={() => this.clearCache()}
            leftIcon={
              <BaseIcon
                containerStyle={{
                  backgroundColor: '#5856D6',
                }}
                icon={{
                  type: 'materialicon',
                  name: 'cached',
                }}
              />
            }
          />
        </View>
        <Text note style={{ alignSelf: 'center', marginTop: 10 }}>Version: {Constants.manifest.version}</Text>
        <DropdownAlert ref={ref => this.dropDownAlertRef = ref} />
      </ScrollView>
    );
  }

  alert = (a, b, c) => this.dropDownAlertRef.alertWithType(a, b, c)

  componentWillMount() {
  }

  componentDidMount() {
    const { navigation } = this.props;
    this.focusListener = navigation.addListener('didFocus', () => {
      if (this.props.navigation.getParam('status')) {
        this.alert('success', 'Succeed', 'Your feedback is sent, please wait admin to response');
      } else {
        this.getData();
      }
    });
  }

  getData = async () => {
    let notification = await AsyncStorage.getItem('notification') || 'true'
    let currency = await AsyncStorage.getItem(ENV.pickerType.currency) || 'hkd'
    let location = await AsyncStorage.getItem(ENV.pickerType.location) || 'hk'
    let language = await AsyncStorage.getItem(ENV.pickerType.language) || 'en-us'
    this.setState({
      pushNotifications: (notification === 'true'),
      currency: currency,
      location: location,
      language: language
    });
  }

  clearCache = () => {
    Alert.alert(
      tran.t('confirm'),
      "Do you really want to delete the cache? It will increase the loading time of the app.",
      [
        {
          text: tran.t('yes'), onPress: async () => {
            await AsyncStorage.getAllKeys().then(async (keys) => {
              const deleteItem = keys.filter(key => key.match(/.*Cache$/g))
              await AsyncStorage.multiRemove(deleteItem)
              FileSystem.deleteAsync(FileSystem.cacheDirectory)
              alert(tran.t('success'))
            })
          }
        },
        { text: tran.t('no'), style: 'cancel' }
      ]
    );
  }

  triggerPicker(type) {
    var data = tran.t(type);
    this.props.navigation.navigate('Picker', { arrayData: data, type: type });
  }

  onPressOptions = (location) => {
    this.props.navigation.navigate(location);
  }

  onChangePushNotifications = async () => {


    this.setState({ pushNotifications: !this.state.pushNotifications })
    await AsyncStorage.setItem('notification', (!this.state.pushNotifications).toString());

    let token = await Notifications.getExpoPushTokenAsync()

    if (this.state.pushNotifications === false) {
      Axios.post(API_HOST_NAME + '/disable-notification', {
        expo_token: token
      })
    }

    if (this.state.pushNotifications === true) {
      Axios.post(API_HOST_NAME + '/token', {
        expo_token: token
      })
    }




  };


}
