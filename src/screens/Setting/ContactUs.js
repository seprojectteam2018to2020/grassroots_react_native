import React, { Component } from 'react'
import { ScrollView, Image } from 'react-native'
import styles from "./styles";
import { Container, Content, Item, Input, Icon } from 'native-base';

export default class ContactUs extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Container>
                <Content>
                    <ScrollView>
                        <Image source={require('../../../assets/logo2x_white.png')} style={styles.headerIcon}></Image>
                        <Item rounded style={{ width: '80%', alignSelf: 'center', marginTop: 30 }}>
                            <Icon active name='phone' type="AntDesign" />
                            <Input placeholder='+852 2342 2345' disabled />
                        </Item>
                        <Item rounded style={{ width: '80%', alignSelf: 'center', marginTop: 30 }}>
                            <Icon active name='mail' type="AntDesign" />
                            <Input placeholder='help@socialize.com' disabled />
                        </Item>
                        <Item rounded style={{ width: '80%', alignSelf: 'center', marginTop: 30 }}>
                            <Icon active name='building' type="FontAwesome" />
                            <Input placeholder='Yuen Long, HK' disabled />
                        </Item>


                    </ScrollView>
                </Content>
            </Container>
        )
    }

    componentWillMount() {

    }

}
