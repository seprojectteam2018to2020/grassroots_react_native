import React, { PureComponent } from 'react';
import { Content, Container, Form, Textarea, Picker, Item, Label, Input, Icon, Text, Left, Body } from 'native-base';
import styles from './styles';
import DropdownAlert from 'react-native-dropdownalert';
import { Button } from 'react-native-elements';
import Constants from 'expo-constants';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions'
import { StackActions, NavigationActions } from 'react-navigation'

export default class Feedback extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            feedbackTypeArray: [],
            type: 0,
            title: "",
            content: "",
            image: {
                type: 'image/jpeg',
                name: 'photo.jpg',
                uri: '#'
            }
        };
    }

    render() {
        let types = this.state.feedbackTypeArray.map((value, index) => {
            return <Picker.Item label={value.name} value={value.id} key={index} />
        })

        return (
            <Container>
                <Content>

                    <Item floatingLabel style={{ width: '90%', marginBottom: 20, alignSelf: 'center' }}>
                        <Label>Title: </Label>
                        <Input
                            returnKeyType={'next'}
                            blurOnSubmit={false}
                            onSubmitEditing={() => { this.refs.contentRef._root.focus(); }}
                            onChangeText={(title) => this.setState({ title })}
                            value={this.state.title}
                        />
                    </Item>

                    <Item style={{ width: '90%', marginBottom: 20, alignSelf: 'center' }}>
                        <Left >
                            <Text style={styles.pickerText}>Type: </Text>
                        </Left>
                        <Body>
                            <Picker
                                mode="dropdown"
                                iosIcon={<Icon name="arrow-down" />}
                                selectedValue={this.state.type}
                                onValueChange={(type) => { this.setState({ type }) }}
                            >
                                <Picker.Item label="Please Select" value={0} />
                                {types}
                            </Picker>
                        </Body>
                    </Item>

                    <Textarea
                        rowSpan={5}
                        bordered
                        ref="contentRef"
                        style={{ width: '90%', alignSelf: 'center' }}
                        placeholder="Content"
                        onChangeText={(content) => this.setState({ content })}
                        value={this.state.content}
                    />

                    <Button
                        buttonStyle={styles.confirmButton}
                        title="Pick Image"
                        onPress={() => this._pickImage()}
                    />

                    <Button
                        buttonStyle={styles.confirmButton}
                        title="Send"
                        onPress={() => this.send()}
                    />

                </Content>
                <DropdownAlert ref={ref => this.dropDownAlertRef = ref} />
            </Container>
        );
    }

    componentDidMount() {
        this.getData()
    }

    alert = (a, b, c) => this.dropDownAlertRef.alertWithType(a, b, c)

    getData = async () => {
        Axios.get(API_HOST_NAME + '/feedback-type-list')
            .then((response) => {
                if (response.status === 200) {
                    this.setState({ feedbackTypeArray: response.data.data })
                } else {
                    this.alert('error', tran.t('network_error'), response.data.message);
                }
            })
            .catch((error) => {
                this.alert('error', tran.t('unexpected_error'), error.message);
            });
    }

    _pickImage = async () => {
        await this.getPermissionAsync();

        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            quality: 0,
            allowsEditing: true
        });

        if (!result.cancelled) {
            this.setState({
                image: {
                    type: 'image/jpeg',
                    name: 'photo.jpg',
                    uri: result.uri
                }
            });

        } else {
            this.setState({
                image: {
                    type: 'image/jpeg',
                    name: 'photo.jpg',
                    uri: '#'
                }
            });
        }
    }

    getPermissionAsync = async () => {
        if (Constants.platform.ios) {
            const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
            if (status !== 'granted') {
                this.alert('error', 'Sorry, we need camera roll permissions to make this work!');
            }
        }
    }

    send() {
        var body = new FormData();

        if (this.state.type == 0) {
            this.alert('warn', 'Please Select your feedback type');
            return;
        } else {
            body.append('type', this.state.type);
        }

        if (this.state.title.trim().length < 5) {
            this.alert('warn', 'Please writing a more richly title');
            return;
        } else {
            body.append('title', this.state.title);
        }

        if (this.state.content.trim().length < 10) {
            this.alert('warn', 'Please writing a more richly content');
            return;
        } else {
            body.append('content', this.state.content);
        }

        if (this.state.image.uri != '#') {
            body.append('image', this.state.image);
        }

        Axios.post(API_HOST_NAME + '/feedback', body)
            .then((response) => {
                if (response.status === 200) {
                    this.props.navigation.dispatch(StackActions.reset({
                        index: 0,
                        actions: [
                            NavigationActions.navigate({ routeName: 'Setting', params: { status: 'Send' } }),
                        ],
                    }))
                } else {
                    this.alert('error', tran.t('network_error'), response.data.message);
                }
            })
            .catch((error) => {
                this.alert('error', tran.t('unexpected_error'), error.message);
            });
    }


}
