import React, { PureComponent } from 'react';
import { Alert, View, AsyncStorage } from 'react-native';
import { ListItem } from 'react-native-elements'
import { Updates } from 'expo';
import styles from "./styles";

export default class ChooseLanguagePage extends PureComponent {

    constructor(props) {
        super(props);
    }

    render() {
        let items = this.props.navigation.getParam('arrayData', []).map((item, index) => {
            return <ListItem
                key={index}
                title={item.title}
                containerStyle={styles.listItemContainer}
                onPress={() => this.onPressOptions(item.title, item.value)}
            />
        })

        return (
            <View>
                {items}
            </View>
        );
    }

    onPressOptions = async (title, option) => {
        const { navigation } = this.props;
        let currency = await AsyncStorage.getItem(ENV.pickerType.currency) || 'hkd'
        let location = await AsyncStorage.getItem(ENV.pickerType.location) || 'hk'
        switch (navigation.getParam('type')) {
            case ENV.pickerType.language:
                asking = tran.t('change_language_content', { origin: tran.t(tran.locale), new: title })
                success = tran.t('change_language_success');
                break;
            case ENV.pickerType.currency:
                asking = tran.t('change_currency_content', { origin: tran.t(currency), new: title })
                success = tran.t('change_currency_success');
                break;
            case ENV.pickerType.location:
                asking = tran.t('change_location_content', { origin: tran.t(location), new: title })
                success = tran.t('change_location_success');
                break;
        }

        Alert.alert(
            tran.t('confirm'),
            asking,
            [
                {
                    text: tran.t('yes'), onPress: async () => {
                        await AsyncStorage.setItem(navigation.getParam('type'), option);
                        Alert.alert(tran.t('success'), success);
                        if (navigation.getParam('type') == ENV.pickerType.language) {
                            Updates.reload();
                        } else {
                            this.props.navigation.navigate('Setting');
                        }
                    }
                },
                { text: tran.t('no'), style: 'cancel' }
            ]
        );
    }
}