import React, { PureComponent } from 'react';
import { ListItem } from 'react-native-elements'
import { ScrollView, FlatList } from 'react-native'
import styles from '../Profile/styles';
import { Text, Container, Icon } from 'native-base';
import { Placeholder, PlaceholderMedia, PlaceholderLine, ShineOverlay } from "rn-placeholder";
import DropdownAlert from 'react-native-dropdownalert';

export default class User extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            userArray: [],
            isReady: false
        }

    }

    keyExtractor = (item, index) => index.toString()

    renderItem = ({ item }) => (
        <ListItem
            onPress={() => { this.props.navigation.navigate('ShowUser', { id: item.id }) }}
            title={item.name.first + " " + item.name.last}
            subtitle={item.email}
            leftAvatar={{
                source: item.avatar && { uri: item.avatar }
            }}
            rightElement={
                (item.status === 0) ?
                    <Icon name="warning" type="Entypo" style={{ fontSize: 18, color: color.yellow }} />
                    : (item.status === 1) ?
                        <Icon name="checkcircle" type="AntDesign" style={{ fontSize: 18, color: color.green }} />
                        : (item.status === 2) ?
                            <Icon name="circle-with-cross" type="Entypo" style={{ fontSize: 18, color: color.red }} />
                            : <Text></Text>
            }
        />
    )

    render() {

        let placeholder = [...Array(8)].map((value, index) => {
            return <Placeholder
                key={index}
                Animation={ShineOverlay}
                Left={PlaceholderMedia}
                style={{ padding: 10 }}
            >
                <PlaceholderLine />
                <PlaceholderLine width={80} />
            </Placeholder>
        })

        return (
            <Container>
                {(this.state.isReady) ?
                    <ScrollView>
                        <FlatList
                            keyExtractor={this.keyExtractor}
                            data={this.state.userArray}
                            renderItem={this.renderItem}
                        />
                    </ScrollView> :
                    <ScrollView>
                        {placeholder}
                    </ScrollView>
                }
                <DropdownAlert ref={ref => this.dropDownAlertRef = ref} />
            </Container>
        );
    }

    alert = (a, b, c) => this.dropDownAlertRef.alertWithType(a, b, c)

    componentWillMount() {

    }

    getData() {
        Axios.get(API_HOST_NAME + '/admin/user/list')
            .then((response) => {
                if (response.status === 200) {
                    this.setState({ userArray: response.data.data, isReady: true })
                } else {
                    this.alert('error', tran.t('network_error'), response.data.message);
                }

            })
            .catch((error) => {
                this.alert('error', tran.t('unexpected_error'), error.message);
            });
    }

    componentDidMount() {

        this.focusListener = this.props.navigation.addListener('didFocus', () => {
            this.getData()
        });

    }
}