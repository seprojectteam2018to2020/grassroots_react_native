import { StyleSheet } from 'react-native';


const styles = StyleSheet.create({
    Buttons: {
        width: 150,
        backgroundColor: '#FFBA64',
        borderRadius: 5,
        height: 45,
        marginTop: 10,
        marginBottom: 20,
    },
    containerView: {
        flex: 1,
    },
    screenContainer: {
        flex: 1,

    },
    headerIcon: {
        width: "60%",
        height: 80,
        marginTop: 50,
        marginBottom: 30,
        alignSelf: 'center',
    },
    logoText: {
        fontSize: 40,
        fontWeight: "800",
        marginTop: 50,
        marginBottom: 30,
        textAlign: 'center',
    },
    formView: {
        flex: 1,
    },
    normalText: {
        marginTop: 30,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around',
        flexWrap: 'wrap'
    },
    formTextInputHalf: {
        height: 43,
        fontSize: 14,
        paddingLeft: 10,
        marginTop: 5,
        marginBottom: 5,
        width: '42%',
    },
    formTextInput: {
        height: 43,
        fontSize: 14,
        paddingLeft: 10,
        marginLeft: 15,
        marginRight: 15,
        marginTop: 10,
        marginBottom: 10,
        width: '92%',
    },
    label: {
        color: '#ff9000',
        bottom: 5,
        left: 4,
    },
    resigterButton: {
        width: '90%',
        backgroundColor: '#ff9000',
        borderRadius: 5,
        height: 45,
        marginTop: 40,
        marginBottom: 40,
        alignSelf: 'center'
    },
    birth: {
        fontSize: 24,
        color: '#ff9000'
    },
    birthView: {
        flex: 1,
        flexDirection: 'row',
    },
    imgwrapper: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
        marginBottom: 50,
    },
    circleWrapper: {
        backgroundColor: '#ECECEC',
        height: 22,
        width: 22,
        borderWidth: 3,
        borderColor: '#ffffff',
        borderRadius: 11,
        marginLeft: 60,
        marginTop: -75,
        alignItems: 'center',
        justifyContent: 'center',
    },
    datePicker: {
        alignSelf: 'flex-start',
        width: 250,
    },

});

export default styles;
