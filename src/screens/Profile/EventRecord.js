import React, { PureComponent } from 'react';
import { ScrollView, View, } from 'react-native'
import styles from '../Profile/styles';
import { Badge } from 'react-native-elements'
import { Body, Text, List, ListItem, Container, Right } from 'native-base';
import DropdownAlert from 'react-native-dropdownalert';

export default class EventRecord extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      recordArray: []
    }

  }

  render() {


    let records = this.state.recordArray.map((value, index) => {
      return <ListItem avatar key={index} thumbnail>
        <Body>
          <Text>{value.title}</Text>
          <Text note>{value.created_at}</Text>
        </Body>
        <Right>
          <Badge status={ENV.event_status[value.status].type} value={ENV.event_status[value.status].name} />
        </Right>
      </ListItem>
    })

    return (
      <Container>
        {
          (records.length != 0) ?
            <ScrollView>
              <List>
                {records}
              </List>
            </ScrollView>
            :
            <View style={{
              flex: 1,
              justifyContent: 'center'
            }}>
              <Text note style={{ textAlign: 'center' }}>You do not have record yet.</Text>
              <Text note style={{ textAlign: 'center' }}>You can join the activites form event page.</Text>
            </View>
        }
        <DropdownAlert ref={ref => this.dropDownAlertRef = ref} />
      </Container>
    );
  }

  componentWillMount() {

  }

  alert = (a, b, c) => this.dropDownAlertRef.alertWithType(a, b, c)

  getData() {
    Axios.get(API_HOST_NAME + '/event-record')
      .then((response) => {
        if (response.status === 200) {
          this.setState({ recordArray: response.data.data })
        } else {
          this.alert('error', tran.t('network_error'), response.data.message);
        }
      })
      .catch((error) => {
        this.alert('error', tran.t('unexpected_error'), error.message);
      });
  }

  componentDidMount() {
    this.getData();
  }
}