import React, { PureComponent } from 'react';
import { ScrollView, } from 'react-native'
import styles from '../Profile/styles';
import DropdownAlert from 'react-native-dropdownalert';
import { Body, Text, List, ListItem, Container, Content, Right } from 'native-base';

export default class PointRecord extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      recordArray: []
    }

  }

  render() {
    let records = this.state.recordArray.map((value, index) => {
      return <ListItem avatar key={index} thumbnail>
        <Body>
          <Text>{value.content}</Text>
          <Text note>{value.created_at}</Text>
        </Body>
        <Right>
          {(value.type === 1)
            ?
            <Text note style={{ color: color.green, fontSize: 18 }}>+{value.points}</Text>
            :
            <Text note style={{ color: color.red, fontSize: 18 }}>-{value.points}</Text>
          }

        </Right>
      </ListItem>

    })
    return (
      <Container>
        <Content>
          <ScrollView>
            <List>
              {records}
            </List>
          </ScrollView>
        </Content>
        <DropdownAlert ref={ref => this.dropDownAlertRef = ref} />
      </Container>
    );
  }

  componentWillMount() {

  }

  alert = (a, b, c) => this.dropDownAlertRef.alertWithType(a, b, c)

  getData() {
    Axios.get(API_HOST_NAME + '/point-record')
      .then((response) => {
        if (response.status === 200) {
          this.setState({ recordArray: response.data.data })
        } else {
          this.alert('error', tran.t('network_error'), response.data.message);
        }
      })
      .catch((error) => {
        this.alert('error', tran.t('unexpected_error'), error.message);
      });
  }

  componentDidMount() {
    this.getData()
  }
}