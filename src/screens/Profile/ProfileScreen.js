import React, { PureComponent } from 'react';
import { Image, TouchableOpacity, View } from 'react-native'
import styles from '../Profile/styles';
import { Button as RNButton } from 'react-native-elements';
import { Left, Body, Text, List, ListItem, Container, Icon, Input } from 'native-base';
import DropdownAlert from 'react-native-dropdownalert';
import * as ImagePicker from 'expo-image-picker';
import DatePicker from 'react-native-datepicker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CacheImage from '../../components/CacheImage';
import { connect } from 'react-redux';

class ProfileScreen extends PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      profileArray: {},
      oldProfileArray: {},
      new_password: '',
      c_new_password: '',
      editMode: false,
      changeIcon: false
    }

  }

  render() {

    const profileArray = this.state.profileArray;
    return (
      <Container>
        <KeyboardAwareScrollView enableOnAndroid={true}>
          <List>
            <ListItem avatar style={{ marginBottom: 15 }}>
              <Left>
                {(this.state.editMode) ?
                  <View style={styles.imgwrapper}>
                    <TouchableOpacity
                      onPress={this._pickImage}
                    >
                      <CacheImage source={{ uri: this.state.profileArray.avatar }} style={{ width: 80, height: 80, borderRadius: 40 }} />
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={styles.circleWrapper}
                      onPress={this._pickImage}
                    >
                      <Text>+</Text>
                    </TouchableOpacity>
                  </View>
                  :
                  <CacheImage source={{ uri: this.state.profileArray.avatar }} style={{ width: 80, height: 80, borderRadius: 40 }} />}
              </Left>
              <Body style={{ paddingLeft: "5%" }}>
                <Text style={(this.state.editMode) ? { color: '#828282' } : {}}>{this.state.profileArray.first_name} {this.state.profileArray.last_name}</Text>
              </Body>
            </ListItem >


            <ListItem icon style={{ marginBottom: 25 }}>
              <Left>
                <Icon type="MaterialCommunityIcons" name="email" />
              </Left>
              <Body>
                <Text style={(this.state.editMode) ? { color: '#828282' } : {}}> {this.state.profileArray.email}</Text>
              </Body>

            </ListItem>

            <ListItem icon style={{ marginBottom: 25 }}>
              <Left>
                <Icon type="FontAwesome" name="phone" />
              </Left>
              <Body>
                {(this.state.editMode) ?
                  <Input
                    onChangeText={(value) => { this.setState({ profileArray: { ...profileArray, phone: value.trim() } }) }}
                    value={this.state.profileArray.phone}
                    placeholder="Phone"
                    keyboardType="numeric"
                    blurOnSubmit={false}
                    returnKeyType='next' />
                  :
                  <Text> {this.state.profileArray.phone}</Text>
                }
              </Body>

            </ListItem>

            <ListItem icon style={(this.state.editMode) ? {} : { marginBottom: 25 }}>
              <Left>
                <Icon type="FontAwesome" name="calendar" />
              </Left>
              <Body>
                {(this.state.editMode) ?
                  <DatePicker
                    showIcon={false}
                    style={styles.datePicker}
                    date={this.state.profileArray.birth}
                    mode="date"
                    maxDate={new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate()}
                    format="YYYY-MM-DD"
                    confirmBtnText={tran.t('confirm')}
                    cancelBtnText={tran.t('cancel')}
                    onDateChange={(value) => { this.setState({ profileArray: { ...profileArray, birth: value } }) }}
                  />
                  :
                  <Text> {this.state.profileArray.birth} </Text>}
              </Body>

            </ListItem>

            {(this.state.editMode) ?
              <Text></Text>
              :
              <ListItem icon style={{ marginBottom: 25 }}>
                <Left>
                  <Icon type="MaterialIcons" name="attach-money" />
                </Left>
                <Body>
                  <Text> {this.state.profileArray.point} points</Text>
                </Body>

              </ListItem>
            }


            <ListItem icon style={{ marginBottom: 25 }}>
              <Left>
                <Icon type="Entypo" name="home" />
              </Left>
              <Body>
                {(this.state.editMode) ?
                  <Input
                    onChangeText={(value) => { this.setState({ profileArray: { ...profileArray, address: value } }) }}
                    value={this.state.profileArray.address}
                    placeholder="Address"
                    blurOnSubmit={false}
                    returnKeyType='next'
                    onSubmitEditing={() => {
                      this.refs.Remarks._root.focus();
                    }} />
                  :
                  <Text> {this.state.profileArray.address}</Text>
                }

              </Body>

            </ListItem>
            {(this.state.editMode) ?
              <ListItem icon style={{ marginBottom: 25 }}>
                <Left>
                  <Icon type="FontAwesome" name="sticky-note-o" />
                </Left>
                <Body>
                  <Input
                    onChangeText={(value) => { this.setState({ profileArray: { ...profileArray, remarks: value } }) }}
                    value={this.state.profileArray.remarks}
                    placeholder="Remarks"
                    blurOnSubmit={false}
                    ref='Remarks'
                    returnKeyType='next'
                    onSubmitEditing={() => {
                      this.refs.Password._root.focus();
                    }} />
                </Body>
              </ListItem>

              : (this.state.profileArray.remarks) ?
                <ListItem icon style={{ marginBottom: 25 }}>
                  <Left>
                    <Icon type="FontAwesome" name="sticky-note-o" />
                  </Left>
                  <Body>
                    <Text> {this.state.profileArray.remarks}</Text>
                  </Body>

                </ListItem>
                : <ListItem />}

            {(this.state.editMode) ?
              <ListItem icon style={{ marginBottom: 25 }}>
                <Left>
                  <Icon type="AntDesign" name="lock" />
                </Left>
                <Body>
                  <Input
                    onChangeText={(value) => { this.setState({ new_password: value }) }}
                    value={this.state.new_password}
                    placeholder="New Password"
                    secureTextEntry={true}
                    blurOnSubmit={false}
                    ref='Password'
                    returnKeyType='next'
                    onSubmitEditing={() => {
                      this.refs.CPassword._root.focus();
                    }} />
                </Body>
              </ListItem>
              : <Text></Text>
            }

            {(this.state.editMode) ?
              <ListItem icon style={{ marginBottom: 25 }}>
                <Left>
                  <Icon type="AntDesign" name="lock" />
                </Left>
                <Body>
                  <Input
                    onChangeText={(value) => { this.setState({ c_new_password: value }) }}
                    value={this.state.c_new_password}
                    placeholder="Confirm New Password"
                    secureTextEntry={true}
                    ref='CPassword'
                    returnKeyType='done' />
                </Body>
              </ListItem>
              : <Text></Text>
            }

            {(this.state.editMode) ?
              <ListItem style={{ flex: 1, justifyContent: 'space-around', flexDirection: 'row', flexWrap: 'wrap' }}>
                <RNButton
                  buttonStyle={styles.Buttons}
                  title="Back"
                  onPress={() => { this.setState({ editMode: false }) }}
                />
                <RNButton
                  buttonStyle={styles.Buttons}
                  title="Update"
                  onPress={() => { this.update() }}
                />
              </ListItem>
              : <ListItem style={{ flex: 1, justifyContent: 'space-around', flexDirection: 'row', flexWrap: 'wrap' }}>

                <RNButton
                  buttonStyle={{
                    width: 320,
                    backgroundColor: '#FFBA64',
                    borderRadius: 5,
                    marginBottom: 15
                  }}
                  title="Edit"
                  onPress={() => {
                    this.setState({ editMode: true });
                    setTimeout(() => {
                      this.setState({ ok: true }) // bug  
                    }, 100)
                  }}
                />

                <RNButton
                  buttonStyle={styles.Buttons}
                  title={tran.t('points_record')}
                  onPress={() => { this.props.navigation.navigate('PointRecord') }}
                />
                <RNButton
                  buttonStyle={styles.Buttons}
                  title={tran.t('events_record')}
                  onPress={() => { this.props.navigation.navigate('EventRecord') }}
                />
              </ListItem>}
          </List>
        </KeyboardAwareScrollView>
        <DropdownAlert ref={ref => this.dropDownAlertRef = ref} />
      </Container>
    );
  }



  alert = (a, b, c) => this.dropDownAlertRef.alertWithType(a, b, c)

  getData = async () => {

    Axios.get(API_HOST_NAME + '/profile')
      .then((response) => {
        if (response.status === 200) {
          const { data } = response.data;
          this.setState({
            profileArray: {
              first_name: data.name.first,
              last_name: data.name.last,
              email: data.email,
              phone: data.phone,
              birth: data.birth,
              point: data.point,
              address: data.address,
              remarks: data.remarks,
              avatar: data.avatar
            }
          })

        } else {
          this.alert('error', tran.t('network_error'), response.data.message);
        }
      })
      .catch((error) => {
        this.alert('error', tran.t('unexpected_error'), error.message);
      });
  }

  getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        this.alert('error', 'Sorry, we need camera roll permissions to make this work!');
      }
    }
  }

  _pickImage = async () => {
    await this.getPermissionAsync();

    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: false
    });

    if (!result.cancelled) {
      const profileArray = this.state.profileArray;
      this.setState({
        profileArray: {
          ...profileArray, avatar: result.uri,
        },
        avatar: {
          uri: result.uri,
          type: 'image/jpeg',
          name: 'photo.jpg'
        },
        changeIcon: true
      });
    }
  }

  update = () => {
    var body = new FormData();
    if (this.state.new_password != this.state.c_new_password) {
      this.alert('warn', 'Input Error', 'Password Confirm not correct'); return;
    } else if (this.state.new_password != '') {
      if (JSON.stringify(this.state.profileArray) == JSON.stringify(this.state.oldProfileArray &&
        this.state.new_password.trim() == '' && this.state.changeIcon == false)) {
        this.alert('warn', 'Common Error', 'Please edit something before submitting')
        return;
      }

      body.append('new_password', this.state.new_password);
    }


    for (var key in this.state.profileArray) {
      let input = this.state.profileArray[key];
      if (input != '' && input != -1) {
        if (msg = this.hasError(input, key)) {
          this.alert('warn', 'Input Error', msg); return;

        }
        body.append(key, input);
      } else {
        this.alert('warn', 'Input Error', 'Please input valid data to update your profile')
        return;
      }
    }

    if (this.state.changeIcon)
      body.append('new_avatar', this.state.avatar);

    Axios.post(API_HOST_NAME + '/profile', body)
      .then((response) => {
        if (response.status === 200) {
          this.getData();
          this.alert('success', 'Update Succeed');
          this.setState({ editMode: false, new_password: '', c_new_password: '' })
        } else {
          this.alert('error', tran.t('network_error'), response.data.message);
        }
      })
      .catch((error) => {
        this.alert('error', tran.t('unexpected_error'), error.message);
      });
  }

  hasError = (input, key) => {
    switch (key) {
      case 'new_password':
        if (input.trim().length < 6)
          return "Passward length should be more than 10 words"; break;
      case 'address':
        if (input.trim().split(' ').length <= 1)
          return "Incorrect Address"; break;
      case 'phone':
        if (!input.match(/^\d{8,11}$/g))
          return "Please input valid phone number"; break;
      default:
        return false;
    }


  }

  componentDidMount() {
    this.getData()
  }
}

function mapStateToProps(state) {
  return { profileArray: state.profileArray }
}

function mapDispatchToProps(dispatch) {
  return { updateProfileData: () => dispatch({ type: 'UPDATE' }) }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen)