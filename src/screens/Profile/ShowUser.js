import React, { PureComponent } from 'react';
import { Badge } from 'react-native-elements'
import { Image, ScrollView, Alert, View } from 'react-native'
import styles from '../Profile/styles';
import { Button as RNButton } from 'react-native-elements';
import { Left, Body, Text, List, ListItem, Container, Right, Icon } from 'native-base';
import DropdownAlert from 'react-native-dropdownalert';
import CacheImage from '../../components/CacheImage';

export default class ShowUser extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            profile: {
                role: [],
            },
            name: {
                first: '',
                last: ''
            },
        }

    }

    alert = (a, b, c) => this.dropDownAlertRef.alertWithType(a, b, c)

    render() {
        let role = this.state.profile.role.map((item, index) => {
            return <Badge key={index} containerStyle={{ marginRight: 5, marginTop: 5 }} status={ENV.role.item} value={item} style={styles.badge} />
        })
        return (
            <Container>
                <ScrollView>
                    <List>
                        <ListItem avatar style={{ marginBottom: 10 }}>
                            <Left>
                                <CacheImage source={{ uri: this.state.profile.avatar }} style={{ width: 80, height: 80, borderRadius: 40 }} />
                            </Left>

                            <Body style={{ paddingLeft: "5%" }}>
                                <Text>{this.state.name.first + " " + this.state.name.last}</Text>
                                <View style={{ flexWrap: 'wrap', flexDirection: 'row', justifyContent: 'flex-start' }}>{role}</View>
                            </Body>
                        </ListItem >


                        <ListItem icon style={{ marginBottom: 25 }}>
                            <Left>
                                <Icon type="MaterialCommunityIcons" name="email" />
                            </Left>
                            <Body>
                                <Text> {this.state.profile.email}</Text>
                            </Body>

                        </ListItem>

                        <ListItem icon style={{ marginBottom: 25 }}>
                            <Left>
                                <Icon type="FontAwesome" name="calendar" />
                            </Left>
                            <Body>
                                <Text>{this.state.profile.birth}</Text>
                            </Body>
                        </ListItem>

                        <ListItem icon style={{ marginBottom: 25 }}>
                            <Left>
                                <Icon type="FontAwesome" name="phone" />
                            </Left>
                            <Body>
                                <Text> {this.state.profile.phone}</Text>
                            </Body>

                        </ListItem>

                        <ListItem icon style={{ marginBottom: 25 }}>
                            <Left>
                                <Icon type="MaterialIcons" name="attach-money" />

                            </Left>
                            <Body>
                                <Text> {this.state.profile.point + " Points"} </Text>
                            </Body>

                        </ListItem>

                        <ListItem icon style={{ marginBottom: 25 }}>
                            <Left>
                                <Icon type="Entypo" name="home" />
                            </Left>
                            <Body>
                                <Text> {this.state.profile.address}</Text>
                            </Body>

                        </ListItem>

                        <ListItem icon style={{ marginBottom: 25 }}>
                            <Left>
                                <Icon type="MaterialIcons" name="speaker-notes" />

                            </Left>
                            <Body>
                                <Text> {this.state.profile.remarks || 'NO REMARK ADDED'}</Text>
                            </Body>

                        </ListItem>
                        <ListItem icon style={{ marginBottom: 25 }}>
                            <Body>
                                <Text note>Created At: {this.state.profile.created_at}</Text>
                            </Body>
                            <Right><Text note>{(this.state.profile.added_at)
                                ?
                                "Added At: " + this.state.profile.added_at
                                :
                                ""
                            }</Text>
                            </Right>
                        </ListItem>

                        <View style={{ flex: 1, justifyContent: 'space-around', flexDirection: 'row' }}>

                            {
                                (this.state.profile.status === 0) ?
                                    <RNButton
                                        buttonStyle={[styles.Buttons, { backgroundColor: color.blue }]}
                                        title={tran.t('approve')}
                                        onPress={() => this.approve()}
                                    />
                                    : (this.state.profile.status === 1) ?
                                        <RNButton
                                            buttonStyle={[styles.Buttons, { backgroundColor: color.red }]}
                                            title={tran.t('ban')}
                                            onPress={() => this.ban()}
                                        />
                                        : (this.state.profile.status === 2) ?
                                            <RNButton
                                                buttonStyle={[styles.Buttons, { backgroundColor: color.secondOrange }]}
                                                title={tran.t('unban')}
                                                onPress={() => this.unban()}
                                            /> : <Text></Text>
                            }
                        </View>
                    </List>

                </ScrollView>
                <DropdownAlert ref={ref => this.dropDownAlertRef = ref} />
            </Container>
        );
    }

    componentWillMount() {
    }

    componentDidMount() {
        this.getData()
    }

    getData() {
        Axios.get(API_HOST_NAME + '/admin/user/show/' + this.props.navigation.getParam('id'))
            .then((response) => {

                if (response.status === 200) {
                    this.setState({ profile: response.data.data, name: response.data.data.name })
                } else {
                    this.alert('error', tran.t('network_error'), response.data.message);
                }
            })
            .catch((error) => {
                this.alert('error', tran.t('unexpected_error'), error.message);
            });

    }

    approve = () => {
        Alert.alert(
            tran.t('confirm'),
            "Do you really want to approve this user?",
            [
                {
                    text: tran.t('yes'), onPress: () => {
                        Axios.post(API_HOST_NAME + '/admin/user/approve/' + this.props.navigation.getParam('id'))
                            .then((response) => {
                                if (response.status === 200) {
                                    this.alert('success', tran.t('success'));
                                    this.getData()
                                } else {
                                    this.alert('error', tran.t('network_error'), response.data.message);
                                }
                            })
                            .catch((error) => {
                                this.alert('error', tran.t('unexpected_error'), error.message);
                            });
                    }
                },
                { text: tran.t('no'), style: 'cancel' }
            ]
        );
    }

    ban = () => {
        Alert.alert(
            tran.t('confirm'),
            "Do you really want to ban this user?",
            [
                {
                    text: tran.t('yes'), onPress: () => {
                        Axios.post(API_HOST_NAME + '/admin/user/ban/' + this.props.navigation.getParam('id'))
                            .then((response) => {
                                if (response.status === 200) {
                                    this.alert('success', tran.t('success'));
                                    this.getData()
                                } else {
                                    this.alert('error', tran.t('network_error'), response.data.message);
                                }
                            })
                            .catch((error) => {
                                this.alert('error', tran.t('unexpected_error'), error.message); alert(error)
                            });
                    }
                },
                { text: tran.t('no'), style: 'cancel' }
            ]
        );
    }

    unban = () => {
        Alert.alert(
            tran.t('confirm'),
            "Do you really want to unban this user?",
            [
                {
                    text: tran.t('yes'), onPress: () => {
                        Axios.post(API_HOST_NAME + '/admin/user/unban/' + this.props.navigation.getParam('id'))
                            .then((response) => {
                                if (response.status === 200) {
                                    this.alert('success', tran.t('success'));
                                    this.getData()
                                } else {
                                    this.alert('error', tran.t('network_error'), response.data.message);
                                }
                            })
                            .catch((error) => {
                                this.alert('error', tran.t('unexpected_error'), error.message); alert(error)
                            });
                    }
                },
                { text: tran.t('no'), style: 'cancel' }
            ]
        );
    }
}