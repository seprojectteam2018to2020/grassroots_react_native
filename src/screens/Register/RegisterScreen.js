import React, { Component } from "react";
import styles from "./styles";
import { Image, Text, View, AsyncStorage } from 'react-native';
import { Button } from 'react-native-elements';
import DatePicker from 'react-native-datepicker';
import { Item, Input, Label, Left, Body, Textarea } from 'native-base';
import DropdownAlert from 'react-native-dropdownalert';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Notifications } from 'expo';
import { connect } from 'react-redux';

class RegisterScreen extends Component {
    constructor(props) {
        super(props);
        this.init();
    }

    init() {
        this.state = {
            formData: {
                first_name: '',
                last_name: '',
                email: '',
                password: '',
                c_password: '',
                address: '',
                birth: '',
                phone: '',
                remarks: '',
            }
        }
    }

    render() {
        const formData = this.state.formData;
        return (
            <View style={styles.screenContainer}>
                <KeyboardAwareScrollView style={styles.formView} enableOnAndroid={true}>
                    <Image source={require('../../../assets/logo2x_white.png')} style={styles.headerIcon}></Image>
                    <View style={styles.normalText}>

                        <Item floatingLabel style={styles.formTextInputHalf}>
                            <Label style={styles.label}>{tran.t('first_name')}</Label>
                            <Input
                                onChangeText={(value) => this.setState({ formData: { ...formData, first_name: value.trim() } })}
                                getRef={input => { this.firstNameRef = input }}
                                blurOnSubmit={false}
                                returnKeyType={'next'}
                                onSubmitEditing={() => { this.lastNameRef._root.focus(); }}
                            />
                        </Item>

                        <Item floatingLabel style={styles.formTextInputHalf}>
                            <Label style={styles.label}>{tran.t('last_name')}</Label>
                            <Input
                                onChangeText={(value) => this.setState({ formData: { ...formData, last_name: value.trim() } })}
                                returnKeyType={'next'}
                                getRef={input => { this.lastNameRef = input }}
                                blurOnSubmit={false}
                                onSubmitEditing={() => { this.emailRef._root.focus(); }}
                            />
                        </Item>

                        <Item floatingLabel style={styles.formTextInput}>
                            <Label style={styles.label}>{tran.t('email')}</Label>
                            <Input
                                onChangeText={(value) => this.setState({ formData: { ...formData, email: value.trim() } })}
                                value={this.state.formData.email}
                                keyboardType="email-address"
                                returnKeyType={'next'}
                                getRef={input => { this.emailRef = input }}
                                blurOnSubmit={false}
                                onSubmitEditing={() => { this.passwordRef._root.focus(); }}
                            />
                        </Item>

                        <Item floatingLabel style={styles.formTextInput}>
                            <Label style={styles.label}>{tran.t('password')}</Label>
                            <Input
                                onChangeText={(value) => this.setState({ formData: { ...formData, password: value.trim() } })}
                                value={this.state.formData.password}
                                secureTextEntry={true}
                                getRef={input => { this.passwordRef = input }}
                                returnKeyType={'next'}
                                blurOnSubmit={false}
                                onSubmitEditing={() => { this.cPasswordRef._root.focus(); }}
                            />
                        </Item>

                        <Item floatingLabel style={styles.formTextInput}>
                            <Label style={styles.label}>{tran.t('c_password')}</Label>
                            <Input
                                onChangeText={(value) => this.setState({ formData: { ...formData, c_password: value.trim() } })}
                                value={this.state.formData.c_password}
                                secureTextEntry={true}
                                returnKeyType={'next'}
                                getRef={input => { this.cPasswordRef = input }}
                                blurOnSubmit={false}
                                onSubmitEditing={() => { this.addressRef._root.focus(); }}
                            />
                        </Item>

                        <Item floatingLabel style={styles.formTextInput}>
                            <Label style={styles.label}>{tran.t('address')}</Label>
                            <Input
                                onChangeText={(value) => this.setState({ formData: { ...formData, address: value.trim() } })}
                                returnKeyType={'next'}
                                getRef={input => { this.addressRef = input }}
                                blurOnSubmit={false}
                                onSubmitEditing={() => { this.phoneRef._root.focus(); }}
                            />
                        </Item>

                        <Item floatingLabel style={styles.formTextInput}>
                            <Label style={styles.label}>{tran.t('phone')}</Label>
                            <Input onChangeText={(value) => this.setState({ formData: { ...formData, phone: value.trim() } })}
                                value={this.state.formData.phone}
                                keyboardType="numeric"
                                getRef={input => { this.phoneRef = input }}
                                onSubmitEditing={() => { this.refs.remarksRef._root.focus(); }}
                                returnKeyType={'next'} />
                        </Item>

                        <Textarea
                            rowSpan={5} bordered placeholder={tran.t('remarks')}
                            placeholderTextColor="#ff9000"
                            ref="remarksRef"
                            onChangeText={(value) => this.setState({ formData: { ...formData, remarks: value } })}
                            style={{ marginTop: 10, width: '90%' }}
                        />


                        <View style={styles.pickerView}>
                            <Item>
                                <Left style={{ flex: 0.30 }}>
                                    <Text style={styles.pickerText}>{tran.t('created_at')}</Text>
                                </Left>
                                <Body style={{ flex: 0.7 }}>
                                    <DatePicker
                                        style={styles.datePicker}
                                        date={this.state.formData.birth}
                                        mode="date"
                                        placeholder={tran.t('login_picker_hint')}
                                        maxDate={new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate()}
                                        format="YYYY-MM-DD"
                                        confirmBtnText={tran.t('confirm')}
                                        cancelBtnText={tran.t('cancel')}
                                        customStyles={{
                                            dateIcon: {
                                                position: 'absolute',
                                                left: 0,
                                                top: 4,
                                                marginLeft: 0
                                            },
                                            dateInput: {
                                                marginLeft: 36
                                            },
                                        }}
                                        onDateChange={(value) => { this.setState({ formData: { ...formData, birth: value } }) }}
                                    />
                                </Body>
                            </Item>
                        </View>
                    </View>
                    <Button
                        buttonStyle={styles.resigterButton}
                        title={tran.t('confirm')}
                        onPress={() => this.registerPress()}
                    />
                </KeyboardAwareScrollView>
                <DropdownAlert ref={ref => this.dropDownAlertRef = ref} />
            </View>
        );
    }

    componentWillMount() {
    }

    componentDidMount() {
    }

    alert = (a, b, c) => this.dropDownAlertRef.alertWithType(a, b, c)

    registerPress() {


        for (let key in this.state.formData) {
            let input = this.state.formData[key].trim();
            if (input.length > 0 || key == 'remarks') {
                if (msg = this.hasError(input, key)) {
                    this.alert('warn', 'Input Error', msg); return;
                }
            } else {
                this.alert('warn', 'Input Error', 'Please input information in your ' + ENV.registerFieldName[key] + ' field'); return;
            }
        }

        if (this.state.formData.password != this.state.formData.c_password) {
            this.alert('warn', 'Input Error', 'Password Confirm not correct'); return;
        }

        Axios.post(API_HOST_NAME + '/register', this.state.formData)
            .then(async (response) => {
                if (response.status === 200) {
                    await AsyncStorage.setItem('apiToken', response.data.token);
                    Axios.defaults.headers.common['Authorization'] = 'Bearer ' + response.data.token;

                    const allowPush = await AsyncStorage.getItem('notification') || true;
                    if (allowPush) {
                        const token = await Notifications.getExpoPushTokenAsync();
                        Axios.post(API_HOST_NAME + '/modify-token-user', {
                            expo_token: token,
                            type: 1
                        })
                            .catch((error) => {
                                this.alert('error', tran.t('unexpected_error'), error.message);
                            });
                    }

                    this.props.navigation.navigate('Loading', { type: ENV.loadType.LOGIN });

                } else if (response.status === 219 || response.status === 215) {
                    this.alert('error', '', response.data.message);
                } else if (response.status == 225) {
                    this.alert('error', 'Register Error', response.data.message);
                } else {
                    this.alert('error', tran.t('network_error'), response.data.message);
                }

            })
            .catch((error) => {
                this.alert('error', tran.t('unexpected_error'), error.message);
            });
    }

    hasError = (input, key) => {

        switch (key) {
            case 'password':
                if (input.trim().length < 6)
                    return "Passward length should be more than 6 words"; break;
            case 'address':
                if (input.trim().split(' ').length <= 1)
                    return "Please input a valid address (should contains space between city and street)"; break;
            case 'phone':
                if (!input.match(/^\d{8,11}$/g))
                    return "Please input a valid phone number"; break;
            case 'email':
                if (!input.match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/g))
                    return "Please input a valid email"; break;
            case 'remarks':
                if (input.trim().length > 0 && input.trim().length < 10)
                    return "Remarks should be empty or more than 10 words"; break;
            default:
                return false;
        }
    }

}


function mapStateToProps(state) {
    return { userProfile: state.userProfile }
}

function mapDispatchToProps(dispatch) {
    return { updateProfileData: () => dispatch({ type: 'UPDATE' }) }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen)