import React, { PureComponent } from 'react';
import { ScrollView, TouchableOpacity, RefreshControl } from 'react-native';
import { View, Container, Icon, ListItem, Text, Body, Right, Left } from 'native-base';
import Swipeable from 'react-native-swipeable-row';
import styles from './styles';
import AwesomeAlert from 'react-native-awesome-alerts';
import DropdownAlert from 'react-native-dropdownalert';

export default class NotificationList extends PureComponent {

    constructor(props) {
        super(props);
        this.init();
    }

    init() {
        this.state = {
            notificationArray: [],
            refreshing: false,
            deleteID: -1,
            showAlert: false
        }
    }

    alert = (a, b, c) => this.dropDownAlertRef.alertWithType(a, b, c)

    render() {

        let items = this.state.notificationArray.map((item, index) => {
            const rightButtons = [
                <TouchableOpacity style={styles.deleteButton} onPress={() => { this.setState({ deleteId: item.id, showAlert: true }) }}>
                    <Icon
                        style={{ fontSize: 45, marginLeft: 15, color: "#b32100" }}
                        type="Entypo"
                        name="trash"
                    />
                </TouchableOpacity>
            ];

            return <Swipeable rightButtons={rightButtons} key={index} >
                <ListItem style={styles.expenseListItem}
                    onPress={() => {
                        (item.notification_id) ?
                            this.props.navigation.navigate('Notification', { id: item.notification_id, read: item.read, read_id: item.id }) :
                            this.props.navigation.navigate('Notification', { data: item, read: item.read, read_id: item.id })
                    }}>
                    <Left style={{ flex: 0.05 }}>
                        {(item.read) ?
                            <Text></Text> :
                            <Icon style={{ fontSize: 15, color: color.red }} name="new" type="Entypo" />
                        }
                    </Left>
                    <Body style={{ flex: 0.65 }}>
                        <Text numberOfLines={1}>{item.title}</Text>
                        <Text note numberOfLines={1}>{item.subtitle}</Text>
                    </Body>
                    <Right style={{ flex: 0.3 }}>
                        <Text numberOfLines={1}>{item.created_at}</Text>
                    </Right>
                </ListItem>
            </Swipeable>
        })


        return (
            <Container>
                {
                    (items.length != 0) ?
                        <ScrollView
                            refreshControl={
                                <RefreshControl
                                    progressBackgroundColor={color.secondOrange}
                                    tintColor={color.secondOrange}
                                    refreshing={this.state.refreshing}
                                    onRefresh={this._onRefresh}
                                />
                            }>
                            {items}
                        </ScrollView>
                        :
                        <View style={{
                            flex: 1,
                            justifyContent: 'center'
                        }}>
                            <Text note style={{ textAlign: 'center' }}>You do not have any notification.</Text>
                        </View>
                }

                {(items.length != 0) ?
                    <Text note style={{ textAlign: 'center', marginBottom: 10 }}>You can swipe right to delete</Text>
                    : <Text></Text>}
                <DropdownAlert ref={ref => this.dropDownAlertRef = ref} />
                <AwesomeAlert
                    show={this.state.showAlert}
                    title={tran.t('confirm')}
                    message={tran.t('confirm_delete')}
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={true}
                    showCancelButton={true}
                    showConfirmButton={true}
                    cancelText={tran.t('no')}
                    confirmText={tran.t('yes')}
                    titleStyle={{ fontSize: 15, fontWeight: 'bold' }}
                    contentContainerStyle={{ width: ENV.window.width * 0.9 }}
                    messageStyle={{ textAlign: 'center', fontSize: 18 }}
                    cancelButtonStyle={{ width: ENV.window.width * 0.3 }}
                    cancelButtonTextStyle={{ textAlign: 'center' }}
                    confirmButtonStyle={{ width: ENV.window.width * 0.3 }}
                    confirmButtonTextStyle={{ textAlign: 'center' }}
                    confirmButtonColor={color.comfirm}
                    onCancelPressed={() => { this.setState({ showAlert: false }) }}
                    onConfirmPressed={() => { this.deleteItem(); this.setState({ showAlert: false }) }}
                />
            </Container>
        );
    }


    componentWillMount() {

    }

    getData = async () => {
        Axios.get(API_HOST_NAME + '/notification-personal')
            .then((response) => {
                if (response.status === 200) {
                    this.setState({ notificationArray: response.data.data })
                } else {
                    this.alert('error', tran.t('network_error'), response.data.message);
                }
            })
            .catch((error) => {
                this.alert('error', tran.t('unexpected_error'), error.message);
            });
    }

    deleteItem() {
        Axios.delete(API_HOST_NAME + '/notification-personal/' + this.state.deleteId)
            .then((response) => {
                if (response.status === 200) {
                    this.getData();
                } else {
                    this.alert('error', tran.t('network_error'), response.data.message);
                }
            })
            .catch((error) => {
                this.alert('error', tran.t('unexpected_error'), error.message);
            });
    }

    _onRefresh = () => {
        this.setState({ refreshing: true });
        this.getData().then(() => {
            setTimeout(() => { this.setState({ refreshing: false }) }, 1000);
        });
    }

    componentDidMount() {
        this.getData();
    }
}