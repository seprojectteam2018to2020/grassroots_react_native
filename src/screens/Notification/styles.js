import { StyleSheet } from 'react-native';


const styles = StyleSheet.create({
    indicator: {
        justifyContent: 'center',
    },
    htmlStyles: {
        fontSize: 18
    },
    updateButton: {
        backgroundColor: '#1268a1',
        justifyContent: 'center',
        height: '100%'
    },
    deleteButton: {
        backgroundColor: '#ff7557',
        justifyContent: 'center',
        height: '100%'
    },
});

export default styles;
