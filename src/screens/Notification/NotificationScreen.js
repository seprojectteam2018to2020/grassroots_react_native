import React, { PureComponent } from 'react';
import { Image, Modal, TouchableOpacity, ActivityIndicator } from 'react-native';
import { Container, Content, Card, CardItem, Thumbnail, Text, Left, Body } from 'native-base';
import ImageViewer from 'react-native-image-zoom-viewer';
import styles from './styles';
import DropdownAlert from 'react-native-dropdownalert';
import CacheImage from '../../components/CacheImage';

export default class NotificationScreen extends PureComponent {

  constructor(props) {
    super(props);
    this.init();
  }

  init() {
    this.state = {
      showImage: false,
      notification: {},
      personal: false
    }
  }

  render() {
    const image = {
      url: this.state.notification.image
    }

    return (
      <Container>
        <Content>
          <Card style={{ flex: 0 }}>
            <CardItem>
              <Left>
                <Thumbnail source={require('../../../assets/admin.png')} />
                <Body>
                  <Text>Administrator</Text>
                  <Text note>{this.state.notification.date}</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem>
              <Body>
                {(!this.state.notification.image) ?
                  <Text></Text>
                  : (this.state.notification.image.endsWith('/')) ?
                    <Text></Text>
                    :
                    <TouchableOpacity
                      onPress={() => { this.setState({ showImage: true }) }}
                      style={{ height: 200, width: '100%', flex: 1, borderRadius: 10, marginBottom: 10, }}>
                      <Image
                        source={image}
                        style={{ height: 200, width: '100%', flex: 1, borderRadius: 10 }} />
                    </TouchableOpacity>
                }
                <Text style={{ fontSize: 20, fontWeight: "bold", marginBottom: 40 }}>{this.state.notification.title}</Text>
                {(this.state.personal)
                  ? <Text>{this.state.notification.subtitle}</Text>
                  : <Text>{this.state.notification.content}</Text>}


              </Body>
            </CardItem>
          </Card>

        </Content>
        <Modal visible={this.state.showImage} transparent={true}>
          <ImageViewer
            imageUrls={[image]}
            failImageSource="Cannot load the image"
            loadingRender={() => <ActivityIndicator style={styles.indicator} size="large" color="#ff9000" />}
            onClick={() => { this.setState({ showImage: false }) }}
          />
        </Modal>
        <DropdownAlert ref={ref => this.dropDownAlertRef = ref} />
      </Container >
    );
  }

  alert = (a, b, c) => this.dropDownAlertRef.alertWithType(a, b, c)

  markAsRead = (id) => {
    Axios.post(API_HOST_NAME + '/notification/read/' + id)
  }

  componentDidMount() {
    const { navigation } = this.props;
    this.focusListener = navigation.addListener('didFocus', () => {
      if (navigation.getParam("id")) {
        Axios.get(API_HOST_NAME + '/notification/' + navigation.getParam("id"))
          .then((response) => {
            if (response.status === 200) {
              this.setState({ notification: response.data.data })
              setTimeout(() => {
                this.setState({ ok: true }) // bug  
              }, 100)
            } else {
              this.alert('error', tran.t('network_error'), response.data.message);
            }
          })
          .catch((error) => {
            this.alert('error', tran.t('unexpected_error'), error.message);
          });
      } else {
        this.setState({
          notification: navigation.getParam("data"),
          personal: true
        })
      }

      // if (!navigation.getParam("read")) {
      //   this.markAsRead(navigation.getParam("read_id"));
      // }

    });
  }
}