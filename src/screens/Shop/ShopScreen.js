import React, { PureComponent } from 'react';
import { ScrollView, View, } from 'react-native'
import styles from '../Profile/styles';
import { List, Container, Text } from 'native-base';
import { PricingCard, Icon } from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import DropdownAlert from 'react-native-dropdownalert';
import { connect } from 'react-redux';


class ShopScreen extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      itemArray: [],
      showAlert: false,
      exId: -1
    }
  }


  static navigationOptions = ({ navigation }) => {
    return {
      headerRight: (
        <View style={{ flex: 1, flexDirection: 'row', padding: 10, alignItems: 'center' }}>
          <Icon
            //containerStyle={{ marginRight: 5 }}
            name='attach-money'
            color='#000'
          />
          <Text>{USER_POINT}</Text>
        </View>
      ),
    };
  };

  render() {
    let items = this.state.itemArray.map((value, index) => {
      return <PricingCard
        key={index}
        color="#4f9deb"
        title={value.title}
        price={'$' + value.points}
        info={[value.description]}
        button={{ title: 'EXCHANGE', icon: 'monetization-on' }}
        onButtonPress={() => { (value.points <= USER_POINT) ? this.setState({ showAlert: true, exId: value.id }) : this.alert('error', 'Common Error', 'You do not have enough credit to exchange the product') }}
      />;
    })
    return (
      <Container>
        <ScrollView>
          <List>
            {items}
          </List>
        </ScrollView>

        <AwesomeAlert
          show={this.state.showAlert}
          title={tran.t('confirm')}
          message={tran.t('confirm_exchange')}
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={true}
          showCancelButton={true}
          showConfirmButton={true}
          cancelText={tran.t('no')}
          confirmText={tran.t('yes')}
          titleStyle={{ fontSize: 15, fontWeight: 'bold' }}
          contentContainerStyle={{ width: ENV.window.width * 0.9 }}
          messageStyle={{ textAlign: 'center', fontSize: 18 }}
          cancelButtonStyle={{ width: ENV.window.width * 0.3 }}
          cancelButtonTextStyle={{ textAlign: 'center' }}
          confirmButtonStyle={{ width: ENV.window.width * 0.3 }}
          confirmButtonTextStyle={{ textAlign: 'center' }}
          confirmButtonColor={color.comfirm}
          onCancelPressed={() => { this.setState({ showAlert: false }) }}
          onConfirmPressed={() => { this.exchange(); this.setState({ showAlert: false }) }}
        />
        <DropdownAlert ref={ref => this.dropDownAlertRef = ref} />
      </Container>
    );
  }

  componentWillMount() {

  }

  alert = (a, b, c) => this.dropDownAlertRef.alertWithType(a, b, c)
  exchange() {


    Axios.post(API_HOST_NAME + '/shop/' + this.state.exId)
      .then((response) => {
        if (response.status === 200) {
          this.alert('success', 'Success', 'You can check your record in Profile Page');
        } else {
          this.alert('error', tran.t('network_error'), response.data.message);
        }
      })
      .catch((error) => {
        this.alert('error', tran.t('unexpected_error'), error.message);
      });
  }

  getData() {
    Axios.get(API_HOST_NAME + '/shop')
      .then((response) => {
        if (response.status === 200) {
          this.setState({ itemArray: response.data.data })
        } else {
          this.alert('error', tran.t('network_error'), response.data.message);
        }
      })
      .catch((error) => {
        this.alert('error', tran.t('unexpected_error'), error.message);
      });
  }

  componentDidMount() {
    this.getData()
  }
}




function mapStateToProps(state) {
  return { userProfile: state.userProfile }
}

function mapDispatchToProps(dispatch) {
  return { updateProfileData: () => dispatch({ type: 'UPDATE' }) }
}

export default connect(mapStateToProps, mapDispatchToProps)(ShopScreen)