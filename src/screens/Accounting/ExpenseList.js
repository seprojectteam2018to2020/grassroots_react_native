import React, { PureComponent } from 'react';
import { Alert, ScrollView, TouchableOpacity } from 'react-native';
import { View, Container, Icon, ListItem, Text, Body, Right } from 'native-base';
import Swipeable from 'react-native-swipeable-row';
import styles from './styles';
import DropdownAlert from 'react-native-dropdownalert';
import { StackActions, NavigationActions } from 'react-navigation'

export default class ExpenseList extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      expenseArray: [],
    };
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerRight: (
        <Icon
          style={{ marginRight: 10 }
          }
          type='Entypo'
          name='plus'
          onPress={() => {
            navigation.dispatch(StackActions.reset({
              index: 2,
              actions: [
                NavigationActions.navigate({ routeName: 'Accounting' }),
                NavigationActions.navigate({ routeName: 'Expense' }),
                NavigationActions.navigate({ routeName: 'CreateExpense' })
              ],
            }))
          }} />
      ),
    };
  };

  render() {

    const { currentlyOpenSwipeable } = this.state;
    const onOpen = (event, gestureState, swipeable) => {
      if (currentlyOpenSwipeable && currentlyOpenSwipeable !== swipeable) {
        currentlyOpenSwipeable.recenter();
      }

      this.setState({ currentlyOpenSwipeable: swipeable });
    };

    const onClose = () => currentlyOpenSwipeable.recenter();

    let items = this.state.expenseArray.map((item, index) => {
      const rightButtons = [
        <TouchableOpacity onPress={() => {
          this.navigate('EditExpense', { id: item.id });
          currentlyOpenSwipeable.recenter();
        }} style={styles.updateButton}>
          <Icon
            style={{ fontSize: 45, marginLeft: 15, color: "#0d4a73" }}
            type="FontAwesome"
            name="pencil-square-o"
          />
        </TouchableOpacity>,
        <TouchableOpacity style={styles.deleteButton} onPress={() => { this.deleteItem(item.id) }}>
          <Icon
            style={{ fontSize: 45, marginLeft: 15, color: "#b32100" }}
            type="Entypo"
            name="trash"
          />
        </TouchableOpacity>
      ];

      return <Swipeable rightButtons={rightButtons} key={index}
        onRightButtonsOpenRelease={onOpen}
        onRightButtonsCloseRelease={onClose}>
        <ListItem thumbnail style={styles.expenseListItem}
          onPress={() => { this.navigate('ShowExpense', { id: item.id }) }}>

          <Body>
            <Text>{item.title}</Text>
            <Text note numberOfLines={1}>{item.transacted_at}</Text>
          </Body>
          <Right>
            <Text style={{ color: '#1784ce', fontSize: 18 }}>${item.amount}</Text>
          </Right>
        </ListItem>
      </Swipeable>
    })


    return (
      <Container>
        {
          (items.length != 0) ?
            <ScrollView>
              {items}
            </ScrollView>
            :
            <View style={{
              flex: 1,
              justifyContent: 'center'
            }}>
              <Text note style={{ textAlign: 'center' }}>You do not create any receipt yet.</Text>
            </View>
        }
        {(items.length != 0) ?
          <Text note style={{ textAlign: 'center', marginBottom: 10 }}>You can swipe right to edit or delete</Text>
          : <Text></Text>}

        <DropdownAlert ref={ref => this.dropDownAlertRef = ref} />
      </Container>
    );
  }

  alert = (a, b, c) => this.dropDownAlertRef.alertWithType(a, b, c)

  componentWillMount() {
    const { navigation } = this.props;
    this.focusListener = navigation.addListener('didFocus', () => {
      if (this.props.navigation.getParam('status')) {
        this.alert('success', this.props.navigation.getParam('status') + ' Succeed');
        this.updateRecord();
      }
    });

    this.focusListener = navigation.addListener('willBlur', () => {
      if (this.state.currentlyOpenSwipeable) {
        this.state.currentlyOpenSwipeable.recenter();
      }
    });

  }

  componentDidMount() {
    this.updateRecord();
  }

  navigate = (routeName, params = null) => {
    this.props.navigation.dispatch(StackActions.reset({
      index: 2,
      actions: [
        NavigationActions.navigate({ routeName: 'Accounting' }),
        NavigationActions.navigate({ routeName: 'Expense' }),
        NavigationActions.navigate({ routeName: routeName, params: params })
      ],
    }))
  }

  updateRecord() {
    Axios.get(API_HOST_NAME + '/receipt')
      .then((response) => {
        if (response.status === 200) {
          this.setState({ expenseArray: response.data.data })
        } else {
          this.alert('error', tran.t('network_error'), response.data.message);
        }
      })
      .catch((error) => {
        this.alert('error', tran.t('unexpected_error'), error.message);
      });
  }

  deleteItem(id) {
    Alert.alert(
      tran.t('confirm'),
      tran.t('confirm_delete'),
      [
        {
          text: tran.t('yes'), onPress: () => {
            Axios.delete(API_HOST_NAME + '/receipt/' + id)
              .then((response) => {
                if (response.status === 200) {
                  this.updateRecord();
                } else {
                  this.alert('error', tran.t('network_error'), response.data.message);
                }
              })
              .catch((error) => {
                this.alert('error', tran.t('unexpected_error'), error.message);
              });
            this.state.currentlyOpenSwipeable.recenter();
          }
        },
        { text: tran.t('no'), style: 'cancel', onPress: () => { this.state.currentlyOpenSwipeable.recenter(); } }
      ]
    );
  }
}
