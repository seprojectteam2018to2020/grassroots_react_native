import React, { PureComponent } from "react";
import styles from "./styles";
import { Text, View, AsyncStorage } from 'react-native';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions'
import { Button } from 'react-native-elements';
import DatePicker from 'react-native-datepicker';
import { Container, Item, Input, Label, Picker, Icon, Textarea, Right, Left, Body } from 'native-base';
import * as ImagePicker from 'expo-image-picker';
import AutoHeightImage from 'react-native-auto-height-image';
import DropdownAlert from 'react-native-dropdownalert';
import { StackActions, NavigationActions } from 'react-navigation'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

export default class CreateExpense extends PureComponent {
    constructor(props) {
        super(props);
        this.init();
    }

    init() {
        this.state = {
            formData: {
                title: '',
                amount: '',
                location: '-1',
                currency_id: '-1',
                expense_type: '-1',
                payment_type: '-1',
                date: '',
                remarks: '',
            },
            receipt: {
                uri: '#',
                type: 'image/jpeg',
                name: 'photo.jpg'
            }
        }
    }

    render() {
        // let currency = ENV.currency.map((item, index) => {
        //     return <Picker.Item label={item.title} value={item.value} key={index} />
        // })
        const { formData } = this.state;
        return (
            <Container style={styles.containerView} >
                <KeyboardAwareScrollView style={styles.formView} enableOnAndroid={true}>
                    <View style={styles.normalText}>

                        <Item floatingLabel style={styles.formTextInput}>
                            <Label style={styles.label}>{tran.t('title')}</Label>
                            <Input
                                onChangeText={(value) => this.setState({ formData: { ...formData, title: value } })}
                                value={formData.title}
                                getRef={input => { this.titleRef = input }}
                                returnKeyType={'next'}
                                blurOnSubmit={false}
                                onSubmitEditing={() => { this.amountRef._root.focus(); }}
                            />
                        </Item>

                        <Item floatingLabel style={styles.formTextInput}>
                            <Label style={styles.label}>{tran.t('amount')}</Label>
                            <Input
                                onChangeText={(value) => this.setState({ formData: { ...formData, amount: value.trim() } })}
                                blurOnSubmit={false}
                                value={formData.amount}
                                keyboardType="numeric"
                                getRef={input => { this.amountRef = input }}
                                returnKeyType={'next'}
                                onSubmitEditing={() => { this.refs.remarksRef._root.focus(); }}
                            />
                        </Item>

                        <View style={styles.pickerView}>

                            <Item>
                                <Left>
                                    <Text style={styles.pickerText}>Location</Text>
                                </Left>
                                <Right>
                                    <Picker
                                        mode="dropdown"
                                        style={styles.picker}
                                        iosIcon={<Icon name="arrow-down" />}
                                        selectedValue={formData.location}
                                        onValueChange={(value) => this.setState({ formData: { ...formData, location: value } })}
                                    >
                                        <Picker.Item label="Hong Kong" value="1" />
                                        <Picker.Item label="China" value="2" />
                                        <Picker.Item label="USA" value="3" />
                                        <Picker.Item label="Japan" value="4" />
                                    </Picker>
                                </Right>
                            </Item>

                            <Item>
                                <Left>
                                    <Text style={styles.pickerText}>Currency</Text>
                                </Left>
                                <Right>
                                    <Picker
                                        mode="dropdown"
                                        iosIcon={<Icon name="arrow-down" />}
                                        style={styles.picker}
                                        selectedValue={formData.currency_id}
                                        onValueChange={(value) => this.setState({ formData: { ...formData, currency_id: value } })}
                                    >
                                        <Picker.Item label="HKD" value="1" />
                                        <Picker.Item label="RMB" value="2" />
                                        <Picker.Item label="USD" value="3" />
                                        <Picker.Item label="EUR" value="4" />
                                        <Picker.Item label="GBP" value="5" />
                                    </Picker>
                                </Right>
                            </Item>

                            <Item>
                                <Left>
                                    <Text style={styles.pickerText}>Expense Type</Text>
                                </Left>
                                <Right>
                                    <Picker
                                        mode="dropdown"
                                        style={styles.picker}
                                        iosIcon={<Icon name="arrow-down" />}
                                        selectedValue={formData.expense_type}
                                        onValueChange={(value) => this.setState({ formData: { ...formData, expense_type: value } })}
                                    >
                                        <Picker.Item label="Transport" value="1" />
                                        <Picker.Item label="Meal" value="2" />
                                        <Picker.Item label="Purchase" value="3" />
                                        <Picker.Item label="Other" value="4" />
                                    </Picker>
                                </Right>
                            </Item>

                            <Item>
                                <Left>
                                    <Text style={styles.pickerText}>Payment Type</Text>
                                </Left>
                                <Right>
                                    <Picker
                                        mode="dropdown"
                                        style={styles.picker}
                                        iosIcon={<Icon name="arrow-down" />}
                                        selectedValue={formData.payment_type}
                                        onValueChange={(value) => this.setState({ formData: { ...formData, payment_type: value } })}
                                    >
                                        <Picker.Item label="Cash" value="1" />
                                        <Picker.Item label="Cheque" value="2" />
                                        <Picker.Item label="Credit Card" value="3" />
                                        <Picker.Item label="Other" value="4" />
                                    </Picker>
                                </Right>
                            </Item>

                            <Item>
                                <Left style={{ flex: 0.35 }}>
                                    <Text style={styles.pickerText}>Payment Date</Text>
                                </Left>
                                <Body style={{ flex: 0.65 }}>
                                    <DatePicker
                                        style={styles.datePicker}
                                        date={formData.date}
                                        mode="date"
                                        placeholder={tran.t('createExpense_picker_hint')}
                                        format="YYYY-MM-DD"
                                        maxDate={new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate()}
                                        confirmBtnText={tran.t('confirm')}
                                        cancelBtnText={tran.t('cancel')}
                                        customStyles={{
                                            dateIcon: {
                                                position: 'absolute',
                                                left: 0,
                                                top: 4,
                                                marginLeft: 0
                                            },
                                            dateInput: {
                                                marginLeft: 36
                                            },
                                        }}
                                        onDateChange={(value) => { this.setState({ formData: { ...formData, date: value } }) }}
                                    />
                                </Body>
                            </Item>
                        </View>

                        <Textarea
                            rowSpan={5} bordered placeholder={tran.t('remarks')}
                            placeholderTextColor="#ff9000"
                            ref="remarksRef"
                            onChangeText={(value) => this.setState({ formData: { ...formData, remarks: value } })}
                            style={styles.textarea}
                            value={formData.remarks}
                        />



                    </View>

                    <Button
                        buttonStyle={styles.confirmButton}
                        title="Pick Receipt Image"
                        onPress={() => this._pickImage()}
                    />


                    <Button
                        buttonStyle={styles.confirmButton}
                        title={tran.t('confirm')}
                        onPress={() => this.submitForm()}
                    />
                    {(this.state.receipt.uri != '#') ?
                        <View>
                            <Text style={{ color: '#ff9000', fontSize: 18, alignSelf: 'center', marginTop: 15, marginBottom: 10 }}>Receipt Preview</Text>
                            <AutoHeightImage style={{ marginBottom: 30, alignSelf: 'center' }} width={ENV.window.width * 0.9} source={{ uri: this.state.receipt.uri }} />
                        </View>
                        : <Text></Text>}
                </KeyboardAwareScrollView>
                <DropdownAlert ref={ref => this.dropDownAlertRef = ref} />
            </Container >
        );
    }

    alert = (a, b, c) => this.dropDownAlertRef.alertWithType(a, b, c)

    _pickImage = async () => {
        await this.getPermissionAsync();

        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            quality: 0,
            allowsEditing: true
        });

        if (!result.cancelled) {
            this.setState({
                receipt: {
                    type: 'image/jpeg',
                    name: 'photo.jpg',
                    uri: result.uri
                }
            });

        } else {
            this.setState({
                receipt: {
                    type: 'image/jpeg',
                    name: 'photo.jpg',
                    uri: '#'
                }
            });
        }
    }

    getPermissionAsync = async () => {
        if (Constants.platform.ios) {
            const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
            if (status !== 'granted') {
                this.alert('error', 'Sorry, we need camera roll permissions to make this work!');
            }
        }
    }

    componentDidMount = async () => {

        if (this.props.navigation.getParam('photo')) {
            this.setState({
                receipt: {
                    uri: this.props.navigation.getParam('photo').uri,
                    type: 'image/jpeg',
                    name: 'photo.jpg',
                }
            })
        }

        const { formData } = this.state;
        let currency = await AsyncStorage.getItem(ENV.pickerType.currency) || 'hkd'
        let location = await AsyncStorage.getItem(ENV.pickerType.location) || 'hk'
        this.setState({
            formData: {
                ...formData,
                currency_id: ENV.currency_index[currency],
                location: ENV.location_index[location]
            }

        });
    }

    submitForm = () => {
        var body = new FormData();
        for (var key in this.state.formData) {

            let input = this.state.formData[key].trim();
            if (input.length > 0 || key == 'remarks') {
                if (input != '-1') {
                    if (msg = this.hasError(input, key)) {
                        this.alert('warn', 'Input Error', msg); return;
                    }
                    body.append(key, input);
                } else {
                    this.alert('warn', 'Input Error', 'Please pick all data before submitting')
                    return;
                }
            } else {
                this.alert('warn', 'Input Error', 'Please input all data before submitting')
                return;
            }

        }

        if (this.state.receipt.uri != '#') {
            body.append('receipt', this.state.receipt);
        }

        Axios.post(API_HOST_NAME + '/receipt',
            body)
            .then((response) => {
                if (response.status === 200) {
                    this.props.navigation.dispatch(StackActions.reset({
                        index: 1,
                        actions: [
                            NavigationActions.navigate({ routeName: 'Accounting' }),
                            NavigationActions.navigate({ routeName: 'Expense', params: { status: 'Create' } })
                        ],
                    }))
                } else {
                    this.alert('error', tran.t('network_error'), response.data.message);
                }
            })
            .catch((error) => {
                this.alert('error', tran.t('unexpected_error'), error.message);
            });
    }


    hasError = (input, key) => {
        switch (key) {
            case 'title':
                if (input.trim().length <= 0)
                    return "Title should input somthing"; break;
            case 'amount':
                if (!input.trim().match(/\d/g) || input.trim() <= 0)
                    return "Amount should be numeric and larger than 0"; break;
            case 'remarks':
                if (input.trim().length > 0 && input.trim().length < 10)
                    return "Remarks should be empty or more than 10 words"; break;
            default:
                return false;
        }
    }

}
