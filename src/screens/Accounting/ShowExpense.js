import React, { Component } from 'react';
import { ActivityIndicator, Modal as RNModal, TouchableOpacity } from 'react-native';
import { Button, ListItem, Container, Content, Card, CardItem, Text, Icon, Left, Body, Textarea } from 'native-base';
import styles from './styles';
import { Button as RNButton } from 'react-native-elements';
import ImageViewer from 'react-native-image-zoom-viewer';
import Modal, { ModalContent, ModalTitle, ModalButton, ModalFooter, SlideAnimation } from 'react-native-modals';
import DropdownAlert from 'react-native-dropdownalert';
import AwesomeAlert from 'react-native-awesome-alerts';
import CacheImage from '../../components/CacheImage';

export default class ShowExpense extends Component {

    constructor(props) {
        super(props);
        this.init();
    }

    init() {
        this.state = {
            visible: false,
            expenseData: { status: '-1' },
            showImage: false,
            image: {},
            comment: "",
            status: 0,
            showAlert: false,
        };
    }

    render() {
        const { expenseData } = this.state;
        return (
            <Container>
                <Content>
                    <Card>
                        <CardItem>
                            <Left>
                                <Icon style={{ fontSize: 30 }} name="paperclip" type="AntDesign" />
                                <Body>
                                    <Text style={styles.headerText}>{expenseData.title}</Text>
                                    <Text note>{expenseData.remarks}</Text>
                                </Body>
                            </Left>
                        </CardItem>

                        <CardItem cardBody>
                            {(expenseData.image && !expenseData.image.endsWith('/')) ?
                                <TouchableOpacity
                                    style={{ height: 200, width: null, flex: 1 }}
                                    onPress={() => { this.setState({ showImage: true }) }}>
                                    <CacheImage source={{ uri: expenseData.image }} style={{ borderRadius: 5, alignSelf: 'center', height: 200, width: ENV.window.width * 0.9, marginTop: 10 }} />
                                </TouchableOpacity>
                                :
                                <Text></Text>}
                        </CardItem>
                        {(isAdmin)
                            ? <Text></Text>
                            :
                            <CardItem>
                                <Left>
                                    <Button transparent onPress={() => this.showComment()}>
                                        <Icon active name="chatbubbles" />
                                        <Text>Comment</Text>
                                    </Button>
                                </Left>
                            </CardItem>
                        }

                    </Card>
                    <ListItem>
                        <Body>
                            <Text style={styles.columnHeader}>Amount</Text>
                            <Text style={styles.dataText} numberOfLines={1}>${expenseData.amount}</Text>
                        </Body>
                    </ListItem>
                    <ListItem>
                        <Body>
                            <Text style={styles.columnHeader}>Expense Type</Text>
                            <Text style={styles.dataText} numberOfLines={1}>{expenseData.expense_string}</Text>
                        </Body>
                    </ListItem>
                    <ListItem>
                        <Body>
                            <Text style={styles.columnHeader}>Transaction Date</Text>
                            <Text style={styles.dataText} numberOfLines={1}>{expenseData.transacted_at}</Text>
                        </Body>
                    </ListItem>
                    <ListItem>
                        <Body>
                            <Text style={styles.columnHeader}>Location</Text>
                            <Text style={styles.dataText} numberOfLines={1}>{expenseData.location_string}</Text>
                        </Body>
                    </ListItem>
                    <ListItem>
                        <Body>
                            <Text style={styles.columnHeader}>Currency</Text>
                            <Text style={styles.dataText} numberOfLines={1}>{expenseData.currency_string}</Text>
                        </Body>
                    </ListItem>
                    <ListItem>
                        <Body>
                            <Text style={styles.columnHeader}>Payment Type</Text>
                            <Text style={styles.dataText} numberOfLines={1}>{expenseData.payment_string}</Text>
                        </Body>
                    </ListItem>
                    <ListItem>
                        <Body>
                            <Text style={styles.columnHeader}>Remarks</Text>
                            <Text style={styles.dataText}>{expenseData.remarks}</Text>
                        </Body>
                    </ListItem>

                    {(isAdmin && expenseData.status == 0) ?
                        <ListItem style={{ flex: 1, justifyContent: 'space-around', flexDirection: 'row' }}>
                            <RNButton
                                buttonStyle={styles.showExpenseButtons}

                                title={tran.t('approve')}
                                onPress={() => this.setState({ visible: true, status: 3 })}
                            />
                            <RNButton
                                buttonStyle={styles.showExpenseButtons}

                                title={tran.t('disapproved')}
                                onPress={() => this.setState({ visible: true, status: 2 })}
                            />
                        </ListItem>
                        : (expenseData.status == null)
                            ?
                            < ListItem style={{ flex: 1, justifyContent: 'space-around', flexDirection: 'row' }}>
                                <RNButton
                                    buttonStyle={styles.showExpenseButtons}
                                    title={tran.t('edit')}
                                    onPress={() => this.props.navigation.navigate('EditExpense', {
                                        id: this.props.navigation.getParam('id')
                                    })}
                                />
                                <RNButton
                                    buttonStyle={styles.showExpenseButtons}
                                    title={tran.t('claim')}
                                    onPress={() => this.setState({ showAlert: true })}
                                />
                            </ListItem>
                            : <Text></Text>

                    }
                </Content>

                <RNModal visible={this.state.showImage} transparent={true}>
                    <ImageViewer
                        imageUrls={[this.state.image]}
                        failImageSource="Cannot load the image"
                        loadingRender={() => <ActivityIndicator style={styles.indicator} size="large" color="#ff9000" />}
                        onClick={() => { this.setState({ showImage: false }) }} />
                </RNModal>

                <Modal
                    width={0.8}
                    height={0.5}
                    modalTitle={<ModalTitle title={(isAdmin) ? "Write Comment" : "Comments"} />}
                    visible={this.state.visible}
                    modalAnimation={new SlideAnimation({
                        slideFrom: 'bottom',
                    })}
                    swipeDirection={['up', 'down']} // can be string or an array
                    swipeThreshold={200}            // default 100
                    onSwipeOut={(event) => {
                        this.setState({ visible: false });
                    }}

                    footer={
                        (isAdmin) ?
                            <ModalFooter>
                                <ModalButton
                                    text="CANCEL"
                                    onPress={() => { this.setState({ visible: false }) }}
                                />
                                <ModalButton
                                    text="OK"
                                    onPress={() => { this.sendComment() }}
                                />
                            </ModalFooter>
                            :
                            <ModalFooter>
                                <ModalButton
                                    text="OK"
                                    onPress={() => { this.setState({ visible: false }) }}
                                />
                            </ModalFooter>
                    }
                >
                    <ModalContent
                        style={{ width: '98%', height: 230, marginTop: 10, alignSelf: 'center' }}>
                        <Textarea
                            value={this.state.expenseData.comment}
                            disabled={!isAdmin}
                            rowSpan={5} bordered
                            placeholderTextColor="#ff9000"
                            onChangeText={(comment) => this.setState({ comment })}
                            style={{ height: '100%', fontSize: 20 }}
                            returnKeyType={'done'}
                            blurOnSubmit={true}
                        />
                    </ModalContent>
                </Modal>


                <DropdownAlert ref={ref => this.dropDownAlertRef = ref} />
                <AwesomeAlert
                    show={this.state.showAlert}
                    title={tran.t('confirm')}
                    message={tran.t('confirm_claim')}
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={true}
                    showCancelButton={true}
                    showConfirmButton={true}
                    cancelText={tran.t('no')}
                    confirmText={tran.t('yes')}
                    titleStyle={{ fontSize: 15, fontWeight: 'bold' }}
                    contentContainerStyle={{ width: ENV.window.width * 0.9 }}
                    messageStyle={{ textAlign: 'center', fontSize: 18 }}
                    cancelButtonStyle={{ width: ENV.window.width * 0.3 }}
                    cancelButtonTextStyle={{ textAlign: 'center' }}
                    confirmButtonStyle={{ width: ENV.window.width * 0.3 }}
                    confirmButtonTextStyle={{ textAlign: 'center' }}
                    confirmButtonColor={color.comfirm}
                    onCancelPressed={() => { this.setState({ showAlert: false }) }}
                    onConfirmPressed={() => { this.claimAction(); this.setState({ showAlert: false }) }}
                />
            </Container >

        );
    }


    componentWillMount() {
        const { navigation } = this.props;
        this.focusListener = navigation.addListener('didFocus', () => {
            Axios.get(API_HOST_NAME + '/receipt/' + navigation.getParam('id'))
                .then((response) => {
                    this.setState({
                        expenseData: response.data.data,
                        image: { url: response.data.data.image }
                    })
                    setTimeout(() => {
                        this.setState({ ok: true }) // bug  
                    }, 100)

                })
                .catch((error) => {
                    this.alert('error', tran.t('unexpected_error'), error.message);
                    this.props.navigation.navigate('Expense');
                });

        });
    }

    componentDidMount() {
    }

    alert = (a, b, c) => this.dropDownAlertRef.alertWithType(a, b, c)

    claimAction = () => {
        Axios.post(API_HOST_NAME + '/receipt-claim/' + this.props.navigation.getParam('id'))
            .then((response) => {
                if (response.status === 200) {
                    this.props.navigation.navigate('Claim', { status: 'Claim' });
                } else {
                    this.alert('error', tran.t('network_error'), response.data.message);
                }
            })
            .catch((error) => {
                this.alert('error', tran.t('unexpected_error'), error.message);
            });
    }

    showComment = () => {
        if (!this.state.expenseData.comment)
            this.alert('info', 'No Comment Now');
        else
            this.setState({ visible: true });
    }

    sendComment = () => {
        Axios.post(API_HOST_NAME + '/admin/account/comment/' + this.props.navigation.getParam('id'), {
            comment: this.state.comment,
            status: this.state.status,
        })
            .then((response) => {
                if (response.status === 200) {
                    this.setState({ visible: false })
                    this.alert('success', 'Succeed');
                    this.props.navigation.navigate("AdminAccounting");
                } else {
                    this.alert('error', tran.t('network_error'), response.data.message);
                }
            })
            .catch((error) => {
                this.alert('error', tran.t('unexpected_error'), error.message);
            });
    }
}
