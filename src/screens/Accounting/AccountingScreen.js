import React, { PureComponent } from 'react';
import { ScrollView, Image } from 'react-native';
import { Container, ListItem, Left, Body, Footer, FooterTab, Button, Icon, Text, Badge } from 'native-base';
import styles from './styles';
import BaseIcon from '../../components/BaseIcon/Icon';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions'
import Constants from 'expo-constants';

export default class AccountingScreen extends PureComponent {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Container>
        <Image
          source={require('../../../assets/accounting.png')} style={styles.indexImage} />
        <ScrollView>
          <ListItem thumbnail onPress={() => { this.props.navigation.navigate('Expense') }}>
            <Left>
              <BaseIcon
                containerStyle={{
                  backgroundColor: '#ff9000',
                }}
                icon={{
                  type: 'font-awesome',
                  name: 'credit-card',
                }}
              />
            </Left>
            <Body>
              <Text>{tran.t('expenses')}</Text>
              <Text note numberOfLines={1}>{tran.t('expenses_des')}</Text>
            </Body>
          </ListItem>
          <ListItem thumbnail onPress={() => { this.props.navigation.navigate('Claim') }}>
            <Left>
              <BaseIcon
                containerStyle={{
                  backgroundColor: '#ff9000',
                }}
                icon={{
                  type: 'font-awesome',
                  name: 'file-text',
                }}
              />
            </Left>
            <Body>
              <Text>{tran.t('claims')}</Text>
              <Text note numberOfLines={1}>{tran.t('claims_des')}</Text>
            </Body>
          </ListItem>

        </ScrollView>
        <Footer>
          <FooterTab style={{ backgroundColor: "#FFF" }}>
            <Button vertical onPress={() => { this.props.navigation.navigate('Feedback') }}>
              {(false) ? <Badge success><Text></Text></Badge> : <Text></Text>}
              <Icon active name="chatbubbles" style={{ color: "#6b6b6b" }} />
              <Text style={{ color: "#6b6b6b" }}>Feedback</Text>
            </Button>
            <Button vertical onPress={() => {
              this.takePhoto()
            }}>
              <Icon name="camera" style={{ color: "#6b6b6b" }} />
              <Text style={{ color: "#6b6b6b" }}>Receipt</Text>
            </Button>
            <Button vertical onPress={() => { this.props.navigation.navigate('CreateExpense') }}>
              <Icon name="plus" type="Entypo" style={{ color: "#6b6b6b" }} />
              <Text style={{ color: "#6b6b6b" }}>Expense</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }

  getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { camera_roll_status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      const { camera_status } = await Permissions.askAsync(Permissions.CAMERA);

      if (camera_roll_status !== 'granted' && camera_status !== 'granted') {
        this.alert('error', 'Sorry, we need camera roll and camera permissions to make this work!');
      }
    }
  }


  takePhoto = async () => {
    await this.getPermissionAsync();

    let result = await ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      quality: 0,
      allowsEditing: true
    });
    if (!result.cancelled) {
      this.props.navigation.navigate('CreateExpense', { photo: result })
    }
  }

}
