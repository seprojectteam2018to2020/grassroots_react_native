import { StyleSheet, Dimensions } from 'react-native';

const { width: viewportWidth } = Dimensions.get('window');

const styles = StyleSheet.create({
    listItemContainer: {
        height: 55,
        borderWidth: 0.5,
        borderColor: '#ECECEC',
    },
    indexImage: {
        resizeMode: 'contain',
        width: '100%',
        height: 220
    },

    containerView: {
        flex: 1,
        marginBottom: 10,
    },
    screenContainer: {
        flex: 1,
    },
    headerIcon: {
        width: "60%",
        height: 80,
        marginTop: 50,
        marginBottom: 30,
        alignSelf: 'center',
    },
    logoText: {
        fontSize: 40,
        fontWeight: "800",
        marginTop: 50,
        marginBottom: 30,
        textAlign: 'center',
    },
    formView: {
        flex: 1,
    },
    normalText: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around',
        flexWrap: 'wrap'
    },
    formTextInputHalf: {
        height: 43,
        fontSize: 14,
        paddingLeft: 10,
        marginTop: 5,
        marginBottom: 5,
        width: '42%',
    },
    formTextInput: {
        height: 43,
        fontSize: 14,
        paddingLeft: 10,
        marginLeft: 15,
        marginRight: 15,
        marginTop: 10,
        marginBottom: 10,
        width: '92%',
    },
    label: {
        color: '#ff9000',
        bottom: 5,
        left: 4,
    },
    confirmButton: {
        width: '90%',
        backgroundColor: '#ff9000',
        borderRadius: 5,
        height: 45,
        marginTop: 20,
        alignSelf: 'center'
    },
    showExpenseButtons: {
        width: 150,
        backgroundColor: '#ff9000',
        borderRadius: 5,
        height: 45,
        marginTop: 20,
        marginBottom: 40
    },
    datePicker: {
        width: '100%',
        marginTop: 10,
        marginBottom: 10,
    },
    textarea: {
        marginTop: 10,
        width: '90%',
    },
    picker: {
        marginBottom: 10,
        marginTop: 10,
        width: 200
    },
    pickerView: {
        width: '90%',
        alignItems: 'flex-start'
    },
    pickerText: {
        color: '#ff9000',
        fontSize: 16
    },
    dataText: {
        color: '#1784ce',
        fontSize: 18,
    },
    columnHeader: {
        fontSize: 16,
    },
    headerText: {
        fontSize: 18,
        fontWeight: 'bold',
    },
    updateButton: {
        backgroundColor: '#1268a1',
        justifyContent: 'center',
        height: '100%'
    },
    deleteButton: {
        backgroundColor: '#ff7557',
        justifyContent: 'center',
        height: '100%'
    },
    imgwrapper: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
        marginBottom: 80,
    },

});

export default styles;
