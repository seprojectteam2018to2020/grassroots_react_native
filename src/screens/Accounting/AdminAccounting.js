import React, { PureComponent } from 'react';
import { Image, RefreshControl, ScrollView, View } from 'react-native';
import { ListItem } from 'react-native-elements'
import { Container, Tab, Tabs, Text } from 'native-base';
import styles from './styles';
import DropdownAlert from 'react-native-dropdownalert';

export default class AdminAccounting extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            receiptArray: []
        };
    }

    render() {
        const { receiptArray } = this.state;
        approving = receiptArray.filter(data => data.status === 0).map((value, index) => {
            return <ListItem
                key={index}
                onPress={() => { this.props.navigation.navigate('ShowExpense', { id: value.id }) }}
                title={value.title}
                subtitle={value.email}
                rightElement={
                    <Text>$ {value.amount}</Text>
                }
            />;
        })

        approved = receiptArray.filter(data => data.status === 3).map((value, index) => {

            return <ListItem
                key={index}
                onPress={() => { this.props.navigation.navigate('ShowExpense', { id: value.id }) }}
                title={value.title}
                subtitle={value.email}
                rightElement={
                    <Text>$ {value.amount}</Text>
                }
            />

        })

        disapproved = receiptArray.filter(data => data.status === 2).map((value, index) => {

            return <ListItem
                key={index}
                onPress={() => { this.props.navigation.navigate('ShowExpense', { id: value.id }) }}
                title={value.title}
                subtitle={value.email}
                rightElement={
                    <Text>$ {value.amount}</Text>
                }
            />;

        })
        return (
            <Container>
                <Image
                    source={require('../../../assets/accounting.png')} style={styles.indexImage} />

                <View style={{ margin: 14, flex: 1, borderColor: color.secondOrange, borderWidth: 4, borderRadius: 10 }}>
                    <Tabs activeTextStyle={{ color: "#69a0ff" }}
                        tabBarUnderlineStyle={{ backgroundColor: "#69a0ff", borderRadius: 5 }}>
                        <Tab heading="Approving" tabStyle={{ backgroundColor: color.secondOrange }}
                            activeTabStyle={{ backgroundColor: color.secondOrange }}>
                            <ScrollView
                                refreshControl={
                                    <RefreshControl
                                        progressBackgroundColor={color.secondOrange}
                                        tintColor={color.secondOrange}
                                        refreshing={this.state.refreshing}
                                        onRefresh={this._onRefresh}
                                    />
                                }>
                                {approving}
                            </ScrollView>
                        </Tab>
                        <Tab heading="Approved" tabStyle={{ backgroundColor: color.secondOrange }}
                            activeTabStyle={{ backgroundColor: color.secondOrange }}>
                            <ScrollView
                                refreshControl={
                                    <RefreshControl
                                        progressBackgroundColor={color.secondOrange}
                                        tintColor={color.secondOrange}
                                        refreshing={this.state.refreshing}
                                        onRefresh={this._onRefresh}
                                    />
                                }>
                                {approved}
                            </ScrollView>
                        </Tab>
                        <Tab heading="Disapproved" tabStyle={{ backgroundColor: color.secondOrange }}
                            activeTabStyle={{ backgroundColor: color.secondOrange }}>
                            <ScrollView
                                refreshControl={
                                    <RefreshControl
                                        progressBackgroundColor={color.secondOrange}
                                        tintColor={color.secondOrange}
                                        refreshing={this.state.refreshing}
                                        onRefresh={this._onRefresh}
                                    />
                                }>
                                {disapproved}
                            </ScrollView>
                        </Tab>
                    </Tabs>
                </View>
                <DropdownAlert ref={ref => this.dropDownAlertRef = ref} />
            </Container>

        );
    }

    componentWillMount() {

    }

    componentDidMount() {
        this.getData()
    }

    alert = (a, b, c) => this.dropDownAlertRef.alertWithType(a, b, c)

    getData = async () => {
        Axios.get(API_HOST_NAME + '/admin/account/list')
            .then((response) => {
                if (response.status === 200) {
                    this.setState({ receiptArray: response.data.data })
                } else {
                    this.alert('error', tran.t('network_error'), response.data.message);
                }
            })
            .catch((error) => {
                this.alert('error', tran.t('unexpected_error'), error.message);
            });

    }

    _onRefresh = () => {
        this.setState({ refreshing: true });
        this.getData().then(() => {
            setTimeout(() => { this.setState({ refreshing: false }) }, 1000);
        });
    }

}
