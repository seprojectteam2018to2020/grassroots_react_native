import React, { PureComponent } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import * as Permissions from 'expo-permissions';
import { Camera } from 'expo-camera';
import { Icon } from 'native-base';
import { StackActions, NavigationActions } from 'react-navigation'

export default class CameraScreen extends PureComponent {
    state = {
        hasCameraPermission: null,
        type: Camera.Constants.Type.back,
    };

    async componentDidMount() {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({ hasCameraPermission: status === 'granted' });
    }

    navigate = (routeName) => {
        this.props.navigation.dispatch(StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: routeName })],
        }))
    }

    snap = async () => {
        if (this.camera) {
            let photo = await this.camera.takePictureAsync();
            this.props.navigation.navigate('CreateExpense', { photo: photo })
        }
    };

    render() {
        const { hasCameraPermission } = this.state;
        if (hasCameraPermission === null) {
            return <View />;
        } else if (hasCameraPermission === false) {
            return <Text>No access to camera</Text>;
        } else {
            return (
                <View style={{ flex: 1 }}>
                    <Camera style={{ flex: 1 }} type={this.state.type} ref={ref => {
                        this.camera = ref;
                    }}>
                        <View
                            style={{
                                flex: 1,
                                backgroundColor: 'transparent',
                                flexDirection: 'row',
                            }}>
                            <View style={{ height: 80, width: '100%', backgroundColor: '#000', flexDirection: 'row', alignSelf: 'flex-end', justifyContent: 'space-between' }}>
                                <TouchableOpacity
                                    style={{ alignSelf: 'center', marginLeft: 15 }}
                                    onPress={() => {
                                        this.setState({
                                            type:
                                                this.state.type === Camera.Constants.Type.back
                                                    ? Camera.Constants.Type.front
                                                    : Camera.Constants.Type.back,
                                        });
                                    }}>
                                    <Icon name="sync" type="AntDesign" style={{ color: "#FFF", fontSize: 30 }} />

                                </TouchableOpacity>

                                <TouchableOpacity
                                    style={{ alignSelf: 'center' }}
                                    onPress={() => { this.snap() }}>
                                    <Icon name="camera" type="MaterialIcons" style={{ color: "#F00", fontSize: 65, borderWidth: 3, borderColor: '#FFF', borderRadius: 35, }} />

                                </TouchableOpacity>

                                <TouchableOpacity
                                    style={{ alignSelf: 'center', marginRight: 15 }}
                                    onPress={() => { this.navigate('Accounting') }}>
                                    <Icon name="circledown" type="AntDesign" style={{ color: "#FFF", fontSize: 30 }} />

                                </TouchableOpacity>

                            </View>

                        </View>
                    </Camera>
                </View>
            );
        }
    }
}
