import React, { PureComponent } from 'react';
import { ScrollView, View } from 'react-native';
import { Badge } from 'react-native-elements'
import { Container, List, ListItem, Text, Left, Body, Right } from 'native-base';
import BaseIcon from '../../components/BaseIcon/Icon';
import styles from './styles';
import DropdownAlert from 'react-native-dropdownalert';

export default class ClaimList extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            claimArray: []
        };
    }

    render() {
        let items = this.state.claimArray.map((item, index) => {
            return <ListItem thumbnail key={index} style={styles.claimExpenseListItem}
                onPress={() => { this.props.navigation.navigate('ShowExpense', { id: item.id }) }}>
                <Left>
                    <BaseIcon
                        containerStyle={{
                            backgroundColor: '#ff9000',
                        }}
                        icon={{
                            type: 'octicon',
                            name: 'file-directory',
                        }}
                    />
                </Left>
                <Body>
                    <Text>{item.title}</Text>
                    <Text note numberOfLines={1}>{item.transacted_at}</Text>

                </Body>
                <Right>
                    <Text>${item.amount}</Text>

                    <Badge status={ENV.receipt_status[item.status].type} value={ENV.receipt_status[item.status].name} style={styles.badge} />
                </Right>
            </ListItem>
        })

        return (
            <Container>
                {
                    (items.length != 0) ?
                        <ScrollView>
                            <List>
                                {items}
                            </List>
                        </ScrollView>
                        :
                        <View style={{
                            flex: 1,
                            justifyContent: 'center'
                        }}>
                            <Text note style={{ textAlign: 'center' }}>You do not claim any receipt yet.</Text>
                        </View>
                }
                <DropdownAlert ref={ref => this.dropDownAlertRef = ref} />
            </Container>
        );
    }

    alert = (a, b, c) => this.dropDownAlertRef.alertWithType(a, b, c)

    componentDidMount() {
        const { navigation } = this.props;
        this.focusListener = navigation.addListener('didFocus', () => {
            if (this.props.navigation.getParam('status')) {
                this.alert('success', 'Claim Succeed', 'Please wait admin to confirm your expense');
                this.updatelist();
            }
        })

    }

    updatelist = () => {
        Axios.get(API_HOST_NAME + '/list-claim')
            .then((response) => {
                if (response.status === 200) {
                    this.setState({ claimArray: response.data.data })
                } else {
                    this.alert('error', tran.t('network_error'), response.data.message);
                }
            })
            .catch((error) => {
                this.alert('error', tran.t('unexpected_error'), error.message);
            });
    }
}
