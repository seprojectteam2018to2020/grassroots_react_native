import React, { PureComponent } from 'react';
import { View, Container, Content, Card, CardItem, Text, Left, Body, List, ListItem } from 'native-base';
import { Button, Icon } from 'react-native-elements';
import styles from './styles';
import CacheImage from '../../components/CacheImage';
import DropdownAlert from 'react-native-dropdownalert';


export default class EventScreen extends PureComponent {

    constructor(props) {
        super(props);
        this.init();
    }

    init() {
        this.state = {
            id: this.props.navigation.getParam("id"),
            eventData: {},
            button_status: "",
        }
    }

    alert = (a, b, c) => this.dropDownAlertRef.alertWithType(a, b, c)

    render() {
        if (this.state.eventData.type == 2) {
            return (
                <Container>
                    <Content>
                        <Card style={{ flex: 0 }}>
                            <CardItem>
                                <Body>
                                    <CacheImage style={{ height: 500, width: '100%', flex: 1, marginTop: 10 }} source={{ uri: this.state.eventData.image }} />
                                </Body>
                            </CardItem>

                            <Card style={{ margin: 10 }}>
                                <CardItem header>
                                    <Text style={{ fontSize: 30, fontWeight: "bold", marginLeft: 10 }}>{this.state.eventData.title}</Text>
                                </CardItem>
                                <CardItem>
                                    <Body>
                                        <Text style={{ fontSize: 16, marginLeft: 10 }}>{this.state.eventData.content}</Text>
                                    </Body>
                                </CardItem>

                            </Card>

                            <List>
                                <ListItem icon style={{ marginBottom: 30, marginTop: 30 }}>
                                    <Left>
                                        <Icon type="Entypo" name="home" />
                                    </Left>
                                    <Body>
                                        <Text>{this.state.eventData.venue}</Text>
                                    </Body>
                                </ListItem>

                                <ListItem icon style={{ marginBottom: 30 }}>
                                    <Left>
                                        <Icon type="MaterialIcons" name="access-time" />
                                    </Left>
                                    <Body>
                                        <Text>{this.state.eventData.time}</Text>
                                    </Body>
                                </ListItem>

                                <ListItem icon style={{ marginBottom: 30 }}>
                                    <Left>
                                        <Icon type="Octicons" name="person" />
                                    </Left>
                                    <Body>
                                        <Text>{this.state.eventData.limit_of_apply}</Text>
                                    </Body>
                                </ListItem>

                                <ListItem icon style={{ marginBottom: 30 }}>
                                    <Left>
                                        <Icon type="MaterialIcons" name="attach-money" />
                                    </Left>
                                    <Body>
                                        <Text>{this.state.eventData.reward} points</Text>
                                    </Body>
                                </ListItem>


                            </List>
                            <CardItem>
                                <View style={{
                                    flex: 1,
                                    flexDirection: 'row',
                                    justifyContent: 'space-around',
                                }}>
                                    {this.state.button_status == "waiting" ?
                                        <View style={{ flex: 1, alignItems: "center" }}>

                                            <Text style={{ fontSize: 24, fontWeight: "bold", color: "#69a0ff", marginBottom: 30 }}>You have Joined</Text>
                                            <Text style={{ fontSize: 24, fontWeight: "bold", color: "#69a0ff", marginBottom: 30 }}>Please wait for approving</Text>

                                            <Button
                                                buttonStyle={styles.buttons}
                                                title={"cancel"}
                                                onPress={() => this.unjoin()}
                                            />
                                        </View>

                                        : this.state.button_status == "canjoin" ?
                                            <Button
                                                buttonStyle={styles.buttons}
                                                title={tran.t('join')}
                                                onPress={() => this.join()}
                                            />
                                            : this.state.button_status == "full" ?
                                                <Text style={{ fontSize: 24, fontWeight: "bold", color: color.red, marginBottom: 30 }}>Full</Text>
                                                : this.state.button_status == "approved" ?
                                                    <View style={{ flex: 1, alignItems: "center" }}>
                                                        <Text style={{ fontSize: 24, fontWeight: "bold", color: "#69a0ff", marginBottom: 30 }}>You are Approved</Text>
                                                        <Button
                                                            buttonStyle={styles.buttons}
                                                            title={"cancel"}
                                                            onPress={() => this.unjoin()}
                                                        />
                                                    </View>
                                                    : this.state.button_status == "attend" ?
                                                        <Text style={{ fontSize: 24, fontWeight: "bold", color: "#69a0ff", marginBottom: 30 }}>You are Attended</Text>
                                                        : <Text style={{ fontSize: 24, fontWeight: "bold", color: color.red, marginBottom: 30 }}>You are Absent</Text>
                                    }

                                </View>
                            </CardItem>
                        </Card>
                    </Content>
                    <DropdownAlert ref={ref => this.dropDownAlertRef = ref} />
                </Container>
            );
        } else {
            return (
                <Container>
                    <Content>
                        <Card style={{ flex: 0 }}>
                            <CardItem>
                                <Body>
                                    <CacheImage style={{ height: 500, width: '100%', flex: 1, marginTop: 10 }} source={{ uri: this.state.eventData.image }} />
                                </Body>
                            </CardItem>

                            <Card style={{ margin: 10 }}>
                                <CardItem header>
                                    <Text style={{ fontSize: 30, fontWeight: "bold", marginLeft: 10 }}>{this.state.eventData.title}</Text>
                                </CardItem>
                                <CardItem>
                                    <Body>
                                        <Text style={{ fontSize: 16, marginLeft: 10 }}>{this.state.eventData.content}</Text>
                                    </Body>
                                </CardItem>

                            </Card>
                            <List>
                                <ListItem icon style={{ marginBottom: 30 }}>
                                    <Left>
                                        <Icon type="Entypo" name="home" />
                                    </Left>
                                    <Body>
                                        <Text>{this.state.eventData.venue}</Text>
                                    </Body>
                                </ListItem>

                                <ListItem icon style={{ marginBottom: 30 }}>
                                    <Left>
                                        <Icon type="MaterialIcons" name="access-time" />
                                    </Left>
                                    <Body>
                                        <Text>{this.state.eventData.time}</Text>
                                    </Body>
                                </ListItem>
                            </List>

                        </Card>
                    </Content>
                </Container>
            )
        }
    }

    componentWillMount() {

    }

    getData = () => {
        Axios.get(API_HOST_NAME + '/event/' + this.state.id)
            .then((response) => {
                if (response.status === 200) {
                    this.setState({
                        eventData: response.data.data,
                        button_status: response.data.data.button_status,
                    })

                    if (this.state.eventData.number_of_apply == this.state.eventData.limit_of_apply) {
                        this.setState({
                            join: false
                        })
                    }
                } else {
                    this.alert('error', tran.t('network_error'), response.data.message);
                }

            })
            .catch((error) => {
                this.alert('error', tran.t('unexpected_error'), error.message);
            });
    }

    componentDidMount() {
        const { navigation } = this.props;
        this.focusListener = navigation.addListener('didFocus', () => {
            this.getData();

        });
    }

    join() {

        var body = new FormData();

        body.append('id', this.state.id);
        Axios.post(API_HOST_NAME + '/event/' + this.state.id,
            body)
            .then((response) => {

                if (response.status === 200) {
                    this.alert('success', 'Join Succeed', 'Please wait admin to confirm your apply.');
                    this.getData();
                } else {
                    this.alert('error', tran.t('network_error'), response.data.message);
                }
            })
            .catch((error) => {

                this.alert('error', tran.t('unexpected_error'), error.message);
            });


    }

    unjoin() {
        var body = new FormData();

        body.append('id', this.state.id);
        Axios.delete(API_HOST_NAME + '/event/' + this.state.id)
            .then((response) => {
                if (response.status === 200) {
                    this.alert('success', 'Cancel Succeed');
                    this.getData();
                } else {
                    this.alert('error', tran.t('network_error'), response.data.message);
                }
            })
            .catch((error) => {
                this.alert('error', tran.t('unexpected_error'), error.message);
            });
    }
}
