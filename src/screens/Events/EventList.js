import React, { PureComponent } from 'react';
import { RefreshControl, ActivityIndicator, FlatList } from 'react-native';
import { View, Text, Icon } from 'native-base';
import styles from './styles';
import { Card, Button as RNButton, Badge } from 'react-native-elements'
import DropdownAlert from 'react-native-dropdownalert';

export default class EventList extends PureComponent {

    constructor(props) {
        super(props);
        this.init();
    }

    init() {
        this.state = {
            refreshing: false,
            eventArray: [],
            page: 1,
            endPage: 1
        }
    }

    alert = (a, b, c) => this.dropDownAlertRef.alertWithType(a, b, c)

    render() {
        return (
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                paddingTop: 30
            }}>

                <FlatList
                    style={{ width: '100%' }}
                    data={this.state.eventArray}
                    ref="FlatList"
                    onEndReachedThreshold={0.1}
                    onEndReached={() => {
                        if (this.state.page < this.state.endPage) {
                            this.setState({ loading: true })
                            setTimeout(() => {
                                this.setState((prevState, props) => ({
                                    page: prevState.page + 1,
                                    loading: false
                                }));
                                this.getDate();
                            }, 700)
                        }
                    }}
                    keyExtractor={(item, index) => index.toString()}
                    refreshControl={
                        <RefreshControl
                            progressBackgroundColor={color.secondOrange}
                            tintColor={color.secondOrange}
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }
                    renderItem={({ item }) => (
                        <Card
                            title={item.title}
                            image={{ uri: item.image }}>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <Text style={{ marginBottom: 10 }}>
                                    {"TIME: " + item.time}
                                </Text>
                                {(item.type === 1) ?
                                    <Badge badgeStyle={{ height: 20, borderWidth: 0, marginLeft: 10 }} textStyle={{ fontSize: 15 }} status="error" value="Activity" />
                                    : (item.type === 2) ?
                                        <Badge badgeStyle={{ height: 20, borderWidth: 0, marginLeft: 10 }} textStyle={{ fontSize: 15 }} status="primary" value="Lecture" />
                                        : <Text></Text>}
                            </View>
                            <RNButton
                                buttonStyle={{ borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0, backgroundColor: color.secondOrange }}
                                icon={<Icon name="content-paste" type="MaterialCommunityIcons" style={{ marginRight: 10 }} />}
                                onPress={() => { this.props.navigation.push('Event', { id: item.id }) }}
                                titleStyle={{ color: 'black', fontWeight: 'bold' }}
                                title='VIEW DETAIL' />
                        </Card>
                    )}
                />
                <View style={{ height: 25, width: '100%' }}>
                    {(this.state.loading) ? <ActivityIndicator style={styles.indicator} size="small" color="#ff9000" /> : <Text></Text>}
                </View>
            </View>
        );
    }

    componentWillMount() {
    }

    getDate = async () => {
        Axios.get(API_HOST_NAME + '/event?page=' + this.state.page)
            .then((response) => {
                if (response.status === 200) {

                    this.setState((prevState, props) => ({
                        eventArray: prevState.eventArray.concat(response.data.data.data),
                        endPage: response.data.data.last_page
                    }));
                } else {
                    this.alert('error', tran.t('network_error'), response.data.message);
                }
            })
            .catch((error) => {
                this.alert('error', tran.t('unexpected_error'), error.message);
            });
    }

    _onRefresh = () => {
        this.setState({ refreshing: true });
        this.getDate().then(() => {
            setTimeout(() => { this.setState({ refreshing: false }) }, 1000);
        });
    }

    componentDidMount() {
        const { navigation } = this.props;
        this.focusListener = navigation.addListener('didFocus', async () => {
            this.getDate();
        });
    }

}
