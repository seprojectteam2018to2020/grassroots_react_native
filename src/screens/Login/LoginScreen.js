import React, { PureComponent } from "react";
import { Notifications } from 'expo';
import styles from "./styles";
import { AsyncStorage, ActivityIndicator, Platform, Keyboard, View, TouchableWithoutFeedback, Image } from 'react-native';
import { Button } from 'react-native-elements';
import { Item, Input, Text } from 'native-base';
import Constants from 'expo-constants'
import { Ionicons } from '@expo/vector-icons';
import DropdownAlert from 'react-native-dropdownalert';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { connect } from 'react-redux';

class LoginScreen extends PureComponent {

    constructor(props) {
        super(props);
        this.init();
    }

    init() {
        this.state = {
            active: false,
            email: 'user2@test.com',
            password: '123456',
            loginning: false
        }
    }

    alert = (a, b, c) => this.dropDownAlertRef.alertWithType(a, b, c)

    render() {
        return (
            <KeyboardAwareScrollView style={styles.containerView} enableOnAndroid={true}>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <View style={styles.loginScreenContainer}>
                        <View style={styles.loginFormView}>
                            <Image source={require('../../../assets/logo2x.png')} style={styles.headerIcon}></Image>
                            <Item style={styles.loginFormText} error={this.state.emailError}>

                                <Ionicons active name={
                                    Platform.OS === 'ios'
                                        ? 'ios-mail'
                                        : 'md-mail'}
                                    color={(this.state.emailError) ? '#ff0000' : '#ff9000'}
                                    size={25} />

                                <Input
                                    onChangeText={(email) => this.setState({ 'email': email.trim() })}
                                    value={this.state.email}
                                    placeholder={tran.t('email')}
                                    placeholderTextColor="#ffffff"
                                    style={styles.loginFormTextInput}
                                    blurOnSubmit={false}
                                    keyboardType="email-address"
                                    returnKeyType={'next'}
                                    onSubmitEditing={() => {
                                        this.refs.Password._root.focus();
                                    }} />
                            </Item>
                            <Item style={styles.loginFormText} error={this.state.passwordError}>

                                <Ionicons active name={
                                    Platform.OS === 'ios'
                                        ? 'ios-unlock'
                                        : 'md-unlock'}
                                    color={(this.state.passwordError) ? '#ff0000' : '#ff9000'}
                                    size={25} />

                                <Input
                                    onChangeText={(password) => this.setState({ 'password': password.trim() })}
                                    value={this.state.password}
                                    placeholder={tran.t('password')}
                                    secureTextEntry={true}
                                    placeholderTextColor="#ffffff"
                                    style={styles.loginFormTextInput}
                                    ref='Password'
                                    returnKeyType='done' />
                            </Item>
                            <Button
                                loading={this.state.loginning}
                                loadingProps={<ActivityIndicator style={styles.indicator} size="large" color="#FFF" />}
                                buttonStyle={styles.loginButton}
                                onPress={() => this.login()}
                                title={tran.t('login')}
                            />
                            <Text
                                style={styles.registerButton}
                                onPress={() => this.props.navigation.navigate('Register')}
                            >{tran.t('sign_up')}</Text>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
                <Text note style={styles.version}>Version: {Constants.manifest.version}</Text>
                <DropdownAlert ref={ref => this.dropDownAlertRef = ref} />
            </KeyboardAwareScrollView>
        );
    }

    login() {
        this.setState({ emailError: false, passwordError: false, loginning: true });
        if (this.state.email.trim() == '') {
            this.setState({ emailError: true, loginning: false });
            return;
        }
        if (this.state.password.trim() == '') {
            this.setState({ passwordError: true, loginning: false });
            return;
        }

        Axios.post(API_HOST_NAME + '/login', {
            email: this.state.email,
            password: this.state.password
        })
            .then(async (response) => {
                if (response.status === 200) {
                    await AsyncStorage.setItem('apiToken', response.data.token);
                    Axios.defaults.headers.common['Authorization'] = 'Bearer ' + response.data.token;

                    const allowPush = await AsyncStorage.getItem('notification') || true;
                    if (allowPush) {
                        const token = await Notifications.getExpoPushTokenAsync();
                        Axios.post(API_HOST_NAME + '/modify-token-user', {
                            expo_token: token,
                            type: 1
                        })
                    }
                    this.props.updateProfileData()
                    this.props.navigation.navigate('Loading', { type: ENV.loadType.LOGIN });

                } else if (response.status === 219 || response.status === 215) {
                    this.alert('error', '', response.data.message);
                    this.setState({ loginning: false })
                } else {
                    this.alert('error', tran.t('network_error'), response.data.message);
                    this.setState({ loginning: false })
                }
            })
            .catch((error) => {
                this.alert('error', tran.t('unexpected_error'), error.message);
                this.setState({ loginning: false });
            });
    }

}


function mapStateToProps(state) {
    return { userProfile: state.userProfile }
}

function mapDispatchToProps(dispatch) {
    return { updateProfileData: () => dispatch({ type: 'UPDATE' }) }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)
