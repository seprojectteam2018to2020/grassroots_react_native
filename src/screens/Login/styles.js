import { StyleSheet } from 'react-native';
import ENV from '../../../constants/variables';

const styles = StyleSheet.create({
    containerView: {
        flex: 1,
        backgroundColor: '#6781a4',
    },
    headerIcon: {
        width: 250,
        height: 100,
        marginTop: 150,
        marginBottom: 30,
        alignSelf: 'center',
    },
    loginScreenContainer: {
        flex: 1,
    },
    logoText: {
        fontSize: 40,
        fontWeight: "800",
        marginTop: 180,
        marginBottom: 30,
        textAlign: 'center',
    },
    loginFormView: {
        flex: 1
    },
    loginFormText: {
        alignSelf: 'center',
        width: '80%',
        height: 43,
        paddingLeft: 10,
        marginLeft: 15,
        marginRight: 15,
        marginTop: 5,
        marginBottom: 5,
    },
    loginFormTextInput: {
        color: '#ffffff'
    },
    loginButton: {
        width: '80%',
        backgroundColor: '#ff9000',
        borderRadius: 5,
        height: 45,
        marginTop: 50,
        alignSelf: 'center'
    },
    registerButton: {
        alignSelf: 'center',
        color: '#ffffff',
        marginTop: '8%',
        fontSize: 18,
        backgroundColor: 'transparent',
    },
    tranButton: {
    },
    fabButton: {
        backgroundColor: '#ff9000',
    },
    fabButtonText: {
        fontSize: 18,
        color: '#fff'
    },
    version: {
        position: 'absolute',
        alignSelf: 'center',
        marginTop: 10,
        color: '#D3D3D3',
        top: ENV.window.height - 40,
    }
});

export default styles;
