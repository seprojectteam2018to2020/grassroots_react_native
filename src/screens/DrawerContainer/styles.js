import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  content: {
    flex: 1,
    flexDirection: 'row'
  },
  container: {
    flex: 1,
    paddingHorizontal: 20,
    backgroundColor: '#FFBA64'
  },
  avatar: {
    width: 150,
    height: 150,
    borderRadius: 150 / 2,
    alignSelf: 'center'
  },
  name: {
    marginTop: 20,
    fontSize: 20,
    textAlign: 'center'
  },
  profile: {
    marginTop: 50,
    marginBottom: 80,
    alignSelf: 'center',
  }
});

export default styles;
