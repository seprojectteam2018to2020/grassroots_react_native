import React, { Component } from 'react';
import { ScrollView, Text, View, TouchableOpacity } from 'react-native';
import { Badge } from 'react-native-elements'
import PropTypes from 'prop-types';
import styles from './styles';
import MenuButton from '../../components/MenuButton/MenuButton';
import { StackActions, NavigationActions } from 'react-navigation'
import CacheImage from '../../components/CacheImage';
import { connect } from 'react-redux';

class LoginedDrawer extends Component {

  constructor(props) {
    super(props);
    this.init();
  }

  init() {
    this.state = {
      name: "",
      role: [],
    }
  }

  navigate = (routeName) => {
    this.props.navigation.dispatch(StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: routeName })],
    }))
  }

  render() {
    isAdmin = false;
    const { navigation } = this.props;
    let role = this.state.role.map((item, index) => {

      if (item === 'Admin') {
        isAdmin = true;
      }
      return <Badge key={index} badgeStyle={{ height: 25, borderWidth: 0 }} containerStyle={{ margin: 5 }} textStyle={{ fontSize: 18 }} status={ENV.role[item]} value={item} style={styles.badge} />
    })
    return (
      <View style={styles.content}>

        <ScrollView style={styles.container}>
          <View style={styles.profile}>
            <TouchableOpacity onPress={() => {
              this.navigate('Profile');
              navigation.closeDrawer();
            }}>
              <CacheImage source={{ uri: this.state.avatar }} style={styles.avatar} />
            </TouchableOpacity>
            <Text style={styles.name} >{this.state.name.first} {this.state.name.last}</Text>
            <View style={{ flexWrap: 'wrap', flexDirection: 'row', justifyContent: 'center' }}>{role}</View>

          </View>
          <MenuButton
            title={tran.t('_home')}
            source="home"
            onPress={() => {
              this.navigate('Home');
              navigation.closeDrawer();
            }}
          />
          {
            (isAdmin)
              ?
              <MenuButton
                title={tran.t('_user')}
                source="contact"
                onPress={() => {
                  this.navigate('User');
                  navigation.closeDrawer();
                }}
              />
              :
              <MenuButton
                title={tran.t('_profile')}
                source="contact"
                onPress={() => {
                  this.navigate('Profile');
                  navigation.closeDrawer();
                }}
              />
          }
          <MenuButton
            title={tran.t('_shop')}
            source="shop"
            type="Entypo"
            onPress={() => {
              this.navigate('Shop');
              navigation.closeDrawer();
            }}
          />

          <MenuButton
            title={tran.t('_account')}
            source="pie"
            onPress={() => {
              if (isAdmin) {
                this.navigate('AdminAccounting');
                navigation.closeDrawer();

              } else {
                this.navigate('Accounting');
                navigation.closeDrawer();
              }

            }}
          />
          <MenuButton
            title={tran.t('_notification')}
            source="notifications"
            onPress={() => {
              this.navigate('NotificationList');
              navigation.closeDrawer();
            }}
          />
          <MenuButton
            title={tran.t('_event')}
            source="list-box"
            onPress={() => {
              this.navigate('EventList');
              navigation.closeDrawer();
            }}
          />

          <MenuButton
            title={tran.t('_setting')}
            source="settings"
            onPress={() => {
              this.navigate('Setting');
              navigation.closeDrawer();
            }}
          />
          <MenuButton
            title={tran.t('_logout')}
            source="log-out"
            onPress={() => {
              this.logout();
            }}
          />
        </ScrollView>
      </View>
    );
  }

  componentWillMount() {
    Axios.get(API_HOST_NAME + '/profile')
      .then((response) => {
        this.setState({
          name: response.data.data.name,
          avatar: response.data.data.avatar,
          role: response.data.data.role
        })
        global.USER_POINT = response.data.data.point
      })
      .catch((error) => {
        alert(error);
      });

  }

  logout = async () => {
    this.props.navigation.navigate('Loading', { type: ENV.loadType.LOGOUT });
  };
}

function mapStateToProps(state) {
  return { userProfile: state.userProfile }
}

function mapDispatchToProps(dispatch) {
  return { updateProfileData: () => dispatch({ type: 'CLEAR' }) }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginedDrawer)

LoginedDrawer.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired
  })
};
