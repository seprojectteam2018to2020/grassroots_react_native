import React, { Component } from 'react';
import { ScrollView, Text, Image, View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import MenuButton from '../../components/MenuButton/MenuButton';

export default class NonLoginDrawer extends Component {
    render() {
        const { navigation } = this.props;
        return (
            <View style={styles.content}>
                <ScrollView style={styles.container}>

                    <View style={styles.profile}>
                        <TouchableOpacity onPress={() => {
                            navigation.navigate('Login');
                            navigation.closeDrawer();
                        }}>
                            <Image source={require('../../../assets/user.png')} style={styles.avatar} />
                        </TouchableOpacity>
                        <Text style={styles.name} >{tran.t('_login_msg')}</Text>
                    </View>
                    <MenuButton
                        title={tran.t('_home')}
                        source="home"
                        onPress={() => {
                            navigation.navigate('Home');
                            navigation.closeDrawer();
                        }}
                    />
                    <MenuButton
                        title={tran.t('_login')}
                        source="log-in"
                        onPress={() => {
                            navigation.navigate('Login');
                            navigation.closeDrawer();
                        }}
                    />



                </ScrollView>
            </View>
        );
    }

    componentWillMount() {

    }

}

NonLoginDrawer.propTypes = {
    navigation: PropTypes.shape({
        navigate: PropTypes.func.isRequired
    })
};
