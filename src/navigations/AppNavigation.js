import { createDrawerNavigator, createStackNavigator, createAppContainer, createSwitchNavigator } from 'react-navigation';
import MenuImage from '../components/MenuImage/MenuImage';
import React from 'react';

import LoginedDrawer from '../screens/DrawerContainer/LoginedDrawer';
import NonLoginDrawer from '../screens/DrawerContainer/NonLoginDrawer';
import HomeScreen from '../screens/Home/HomeScreen';
import NonLoginHomeScreen from '../screens/Home/NonLoginHomeScreen'

import LoginScreen from '../screens/Login/LoginScreen';
import RegisterScreen from '../screens/Register/RegisterScreen';
import LoadingScreen from '../screens/Loading/LoadingScreen';
import NotificationScreen from '../screens/Notification/NotificationScreen';
import NotificationList from '../screens/Notification/NotificationList';
import ShopScreen from '../screens/Shop/ShopScreen';

import EventScreen from '../screens/Events/EventScreen';
import EventList from '../screens/Events/EventList';


// profile
import ProfileScreen from '../screens/Profile/ProfileScreen';
import EventRecord from '../screens/Profile/EventRecord';
import PointRecord from '../screens/Profile/PointRecord';

// Admin
import User from '../screens/Profile/User';
import ShowUser from '../screens/Profile/ShowUser';
import AdminAccounting from '../screens/Accounting/AdminAccounting';

// setting 
import SettingScreen from '../screens/Setting/SettingScreen';
import PickerScreen from '../screens/Setting/Picker';
import ContactUs from '../screens/Setting/ContactUs';
import Feedback from '../screens/Setting/Feedback';
import Terms from '../screens/Setting/TermsAndPolicies';

// accounting
import AccountingScreen from '../screens/Accounting/AccountingScreen';
import ExpenseList from '../screens/Accounting/ExpenseList';
import ClaimList from '../screens/Accounting/ClaimList';
import CreateExpense from '../screens/Accounting/CreateExpense';
import ShowExpense from '../screens/Accounting/ShowExpense';
import EditExpense from '../screens/Accounting/EditExpense';
import Camera from '../screens/Accounting/Camera';

const LoginedStack = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: ({ navigation }) => ({
        title: tran.t('home'),
        headerLeft: (
          <MenuImage onPress={() => { navigation.openDrawer(); }} />
        ),
      })
    },
    Profile: {
      screen: ProfileScreen,
      navigationOptions: ({ navigation }) => ({
        title: tran.t('profile'),
        headerLeft: (
          <MenuImage onPress={() => { navigation.openDrawer(); }} />
        ),
      })
    },
    User: {
      screen: User,
      navigationOptions: ({ navigation }) => ({
        title: tran.t('user'),
        headerLeft: (
          <MenuImage onPress={() => { navigation.openDrawer(); }} />
        ),
      })
    },
    Setting: {
      screen: SettingScreen,
      navigationOptions: ({ navigation }) => ({
        title: tran.t('setting'), headerLeft: (
          <MenuImage onPress={() => { navigation.openDrawer(); }} />
        ),
      })
    },
    Picker: {
      screen: PickerScreen,
      navigationOptions: () => ({ title: '' })
    },
    Notification: {
      screen: NotificationScreen,
      navigationOptions: () => ({ title: tran.t('notification') })
    },
    NotificationList: {
      screen: NotificationList,
      navigationOptions: ({ navigation }) => ({
        title: tran.t('notification'),
        headerLeft: (
          <MenuImage onPress={() => { navigation.openDrawer(); }} />
        ),
      })
    },
    Accounting: {
      screen: AccountingScreen,
      navigationOptions: ({ navigation }) => ({
        title: tran.t('accounting'),
        headerLeft: (
          <MenuImage onPress={() => { navigation.openDrawer(); }} />
        ),
      })
    },
    AdminAccounting: {
      screen: AdminAccounting,
      navigationOptions: ({ navigation }) => ({
        title: tran.t('accounting'),
        headerLeft: (
          <MenuImage onPress={() => { navigation.openDrawer(); }} />
        ),
      })
    },
    Expense: {
      screen: ExpenseList,
      navigationOptions: ({ navigation }) => ({
        title: tran.t('expense'),
      })
    },
    Claim: {
      screen: ClaimList,
      navigationOptions: () => ({ title: tran.t('claim') })
    },
    CreateExpense: {
      screen: CreateExpense,
      navigationOptions: () => ({ title: tran.t('create') })
    },
    ShowExpense: {
      screen: ShowExpense,
      navigationOptions: () => ({ title: tran.t('show') })
    },
    EditExpense: {
      screen: EditExpense,
      navigationOptions: () => ({ title: tran.t('edit') })
    },
    Feedback: {
      screen: Feedback,
      navigationOptions: () => ({ title: tran.t('feedback') })
    },
    Terms: {
      screen: Terms,
      navigationOptions: () => ({ title: tran.t('terms') })
    },
    ContactUs: {
      screen: ContactUs,
      navigationOptions: () => ({ title: tran.t('contact') })
    },
    Event: {
      screen: EventScreen,
      navigationOptions: () => ({ title: tran.t('event') })
    },
    EventList: {
      screen: EventList,
      navigationOptions: ({ navigation }) => ({
        title: tran.t('event'),
        headerLeft: (
          <MenuImage onPress={() => { navigation.openDrawer(); }} />
        )
      })
    },
    PointRecord: {
      screen: PointRecord,
      navigationOptions: () => ({ title: tran.t('point_record') })
    },
    EventRecord: {
      screen: EventRecord,
      navigationOptions: () => ({ title: tran.t('event_record') })
    },
    Shop: {
      screen: ShopScreen,
      navigationOptions: ({ navigation }) => ({
        title: tran.t('shop'),
        headerLeft: (
          <MenuImage onPress={() => { navigation.openDrawer(); }} />
        ),
      })
    },
    ShowUser: {
      screen: ShowUser,
      navigationOptions: () => ({ title: tran.t('profile') })
    },
    Camera: {
      screen: Camera,
      navigationOptions: () => ({ header: null })
    },

  },
  {
    defaultNavigationOptions: {
      headerTintColor: '#000',
      headerStyle: {
        backgroundColor: '#FFBA64',
      },
    },
    initialRouteName: 'Home',
  }
);

const LoginedDrawerStack = createDrawerNavigator(
  {
    App: LoginedStack
  },
  {
    drawerType: 'back',
    drawerPosition: 'left',
    initialRouteName: 'App',
    drawerWidth: 250,
    contentComponent: LoginedDrawer
  }
);

const NonLoginStack = createStackNavigator(
  {
    Login: {
      screen: LoginScreen,
      navigationOptions: () => ({ header: null })
    },
    Register: {
      screen: RegisterScreen,
      navigationOptions: () => ({ title: tran.t('sign_up') })
    },
    Home: {
      screen: NonLoginHomeScreen,
      navigationOptions: ({ navigation }) => ({
        title: tran.t('home'),
        headerLeft: (
          <MenuImage onPress={() => { navigation.openDrawer(); }} />
        ),
      })
    },
    Setting: {
      screen: SettingScreen,
      navigationOptions: () => ({ title: tran.t('setting') })
    },
    Notification: {
      screen: NotificationScreen,
      navigationOptions: () => ({ title: tran.t('notification') })
    },
    Feedback: {
      screen: Feedback,
      navigationOptions: () => ({ title: tran.t('feedback') })
    },
    Picker: {
      screen: PickerScreen,
      navigationOptions: () => ({ title: '' })
    },
    Event: {
      screen: EventScreen,
      navigationOptions: () => ({ title: tran.t('event') })
    },
  },
  {
    defaultNavigationOptions: {
      headerTintColor: '#000',
      headerStyle: {
        backgroundColor: '#FFBA64',
      },
    },
    initialRouteName: 'Home',

  }
);

const NonLoginDrawerStack = createDrawerNavigator(
  {
    App: NonLoginStack
  },
  {
    drawerType: 'back',
    drawerPosition: 'left',
    initialRouteName: 'App',
    drawerWidth: 250,
    contentComponent: NonLoginDrawer
  }
);

const MainStack = createSwitchNavigator(
  {
    App: LoginedDrawerStack,
    Auth: NonLoginDrawerStack,
    Loading: {
      screen: LoadingScreen,
      navigationOptions: () => ({ header: null })
    },
  },
  {
    initialRouteName: 'Loading',
  }
);

export default AppContainer = createAppContainer(MainStack);

console.disableYellowBox = true;