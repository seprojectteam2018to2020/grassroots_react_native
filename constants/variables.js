import { Dimensions } from 'react-native';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default {
    window: {
        width,
        height,
    },

    pickerType: {
        language: 'language_picker_type',
        currency: 'currency_picker_type',
        location: 'location_picker_type',
    },

    expenseType: [
        'Meals', 'Transports', 'Others'
    ],

    accountingType: {
        expenses: 'expenses',
        claims: 'claims',
    },

    receipt_status: {
        0: { name: 'Waiting For Approving', type: 'primary' },
        1: { name: 'Waiting For Revising', type: 'warning' },
        2: { name: 'Disapproved', type: 'error' },
        3: { name: 'Approved', type: 'success' },
    },

    event_status: {
        0: { name: 'Approving', type: 'primary' },
        1: { name: 'Approved', type: 'success' },
        2: { name: 'Attended', type: 'success' },
    },

    role: {
        'Admin': 'primary',
        'User': 'primary',
        'Staff': 'primary',
        'Member': 'success',
        'Junior Member': 'success',
        'Senior Member': 'success',
    },

    location: {
        0: 'Hong Kong',
        1: 'China',
        2: 'USA',
        3: 'Japan',
    },

    location_index: {
        hk: '1',
        china: '2',
        usa: '3',
        japan: '4',
    },

    currency_index: {
        hkd: '1',
        rmb: '2',
        usd: '3',
        eur: '4',
        gbp: '5',
    },

    loadType: {
        LOGIN: 0,
        LOGOUT: 1
    },

    registerFieldName: {
        first_name: '"First Name"',
        last_name: '"Last Name"',
        email: '"Email"',
        password: '"Password"',
        c_password: '"Confirm Password"',
        address: '"Address"',
        birth: '"Company Created At"',
        phone: '"Phone"',
        remarks: '"Remarks"',
    }

};
