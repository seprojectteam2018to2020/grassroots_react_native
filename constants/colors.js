// https://developer.apple.com/ios/human-interface-guidelines/visual-design/color/

const colors = {
    mainOrange: '#FF9000',
    secondOrange: '#FFBA64',
    loginBlue: '#6781a4',
    blue: '#1784ce',
    green: '#00CC73',
    red: '#ff1100',
    yellow: '#f4ca0d',
    comfirm: '#FFBA64',
}

export default colors
