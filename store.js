import { createStore } from 'redux';
import Axios from 'axios';
global.Axios = Axios;
Axios.defaults.headers.common['Content-Type'] = 'application/json';
Axios.defaults.headers.common['Accept'] = 'application/json';
Axios.defaults.timeout = 5000;

const initialState = { userProfile: [] }
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'UPDATE':
            Axios.get(API_HOST_NAME + '/profile')
                .then((response) => {
                    if (response.status === 200) {
                        return { userProfile: response.data.data }
                    }

                })
            return { userProfile: state.userProfile }
        case 'CLEAR':
            return { userProfile: [] }
        default:
            if (Axios.defaults.headers.common['Authorization']) {
                Axios.get(API_HOST_NAME + '/profile')
                    .then((response) => {
                        if (response.status === 200) {
                            return { userProfile: response.data.data }
                        }
                    })
            } else {
                return state
            }

    }
}
const store = createStore(reducer);

export default store;
