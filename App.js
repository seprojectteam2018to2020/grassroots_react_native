import React, { PureComponent } from 'react';
import { AsyncStorage, NetInfo, Alert } from 'react-native';
import { Notifications, Updates } from 'expo';
import AppContainer from './src/navigations/AppNavigation';
import i18n from 'i18n-js';
import store from './store';
import { Provider } from 'react-redux';

import { API_HOST_NAME } from 'react-native-dotenv';
import ENV from './constants/variables';
import color from './constants/colors';

import en_us from './locales/en_us';
import zh_hk from './locales/zh_hk';
import zh_cn from './locales/zh_cn';

global.isAdmin = false;
global.tran = i18n;
global.color = color;
tran.translations = { zh_hk, en_us, zh_cn };
selectLanguage = async () => {
  const language = await AsyncStorage.getItem(ENV.pickerType.language);
  tran.locale = (language) ? language : 'en_us';
};
this.selectLanguage();

global.API_HOST_NAME = API_HOST_NAME;
global.ENV = ENV;


export default class App extends PureComponent {

  render() {
    return (
      <Provider store={store}>
        <AppContainer />
      </Provider>
    );
  }

  componentWillMount = async () => {
    Notifications.addListener(this._handleNotification);
    NetInfo.addEventListener("connectionChange", this.handleConnectionChange);

    if (!__DEV__) {
      const update = await Updates.checkForUpdateAsync();
      if (update.isAvailable) {
        Updates.fetchUpdateAsync().then(() => {
          Alert.alert(
            tran.t('confirm'),
            "Your version is behind the latest version, do you want to reload the app for updating latest version?",
            [
              {
                text: tran.t('yes'), onPress: () => {
                  Updates.reload();
                }
              },
              { text: tran.t('no'), style: 'cancel' }
            ]
          );
        })
      }
    }

  }

  handleConnectionChange = (connectionInfo) => {
    NetInfo.isConnected.fetch().then(isConnected => {
      if (!isConnected) {
        alert("You do not connect the Network, new data cannot be updated")
      }
    });
  }

  _handleNotification = (notification) => {
    if (notification.origin === 'selected') {
      const data = notification.data;
      this.props.navigation.navigate(data.location, data.params);
    }
  };
}
