const zh_hk = {
    email: '電子郵件',
    password: '密碼',
    login: '登入',
    logout: '登出',
    sign_up: '註冊',
    first_name: '姓氏',
    last_name: '名字',
    c_password: '重新輸入密碼',
    address: '住址',
    phone: '電話號碼',
    login_picker_hint: '選擇日期',
    confirm: '確定',
    cancel: '取消',
    no: '否',
    yes: '是',
    change_language_content: '你確定要從{{origin}}切換到{{new}}嗎?',
    change_language_success: '請重新啟動程式以改變設定。',
    success: '成功',
    zh_hk: "繁體中文",
    zh_cn: "簡體中文",
    en_us: "英文",
    profile: '個人檔案',
    account: '會計',
    setting: '設定',
    created_at: '成立日期',
    remarks: '備註',

    // drawer 
    _login_msg: '登入可觀看更多資訊',
    _home: '主頁',
    _login: '登入',
    _register: '註冊',

    // header
    home: '主頁',

    // home
    new: '消息',
    information: '資訊',
    view: '觀看',
    box_1: '點數紀錄',
    box_2: '活動紀錄',
    box_3: '新增支出',
    box_4: '報銷清單',

    // currency
    usd: 'USD',
    hkd: 'HKD',
    jpy: 'JPY',
    cny: 'CNY',
    mop: 'MOP',

    // location
    hk: 'Hong Kong',
    usa: 'USA',
    japan: 'Japan',
    china: 'China',

    // language
    zh_hk: "Traditional Chinese",
    zh_cn: "Simplified Chinese",
    en_us: "English",

    language_picker_type: [
        { title: "Traditional Chinese", value: 'zh_hk' },
        { title: "Simplified Chinese", value: 'zh_cn' },
        { title: "English", value: 'en_us' },
    ],

    currency_picker_type: [
        { title: 'USD', value: 'usd' },
        { title: 'HKD', value: 'hkd' },
        { title: 'JPY', value: 'jpy' },
        { title: 'CNY', value: 'cny' },
        { title: 'MOP', value: 'mop' },
    ],

    location_picker_type: [
        { title: 'Hong Kong', value: 'hk' },
        { title: 'USA', value: 'usa' },
        { title: 'Japan', value: 'japan' },
        { title: 'China', value: 'china' },

    ],

}

export default zh_hk;