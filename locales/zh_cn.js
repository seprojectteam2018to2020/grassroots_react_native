const zh_cn = {
    email: '电子邮件',
    password: '密码',
    login: '登入',
    logout: '登出',
    sign_up: '注册',
    first_name: '姓氏',
    last_name: '名字',
    c_password: '重新输入密码',
    address: '住址',
    phone: '电话号码',
    login_picker_hint: '选择日期',
    confirm: '确定',
    cancel: '取消',
    no: '否',
    yes: '是',
    change_language_content: '你确定要从{{origin}}切换到{{new}}吗?',
    change_language_success: '请重新启动程式以改变设定。',
    success: '成功',
    zh_hk: "繁体中文",
    zh_cn: "简体中文",
    en_us: "英文",
    profile: '个人档案',
    account: '会计',
    setting: '设定',

    // currency
    usd: 'USD',
    hkd: 'HKD',
    jpy: 'JPY',
    cny: 'CNY',
    mop: 'MOP',

    // location
    hk: 'Hong Kong',
    usa: 'USA',
    japan: 'Japan',
    china: 'China',

    // language
    zh_hk: "Traditional Chinese",
    zh_cn: "Simplified Chinese",
    en_us: "English",

    language_picker_type: [
        { title: "Traditional Chinese", value: 'zh_hk' },
        { title: "Simplified Chinese", value: 'zh_cn' },
        { title: "English", value: 'en_us' },
    ],

    currency_picker_type: [
        { title: 'USD', value: 'usd' },
        { title: 'HKD', value: 'hkd' },
        { title: 'JPY', value: 'jpy' },
        { title: 'CNY', value: 'cny' },
        { title: 'MOP', value: 'mop' },
    ],

    location_picker_type: [
        { title: 'Hong Kong', value: 'hk' },
        { title: 'USA', value: 'usa' },
        { title: 'Japan', value: 'japan' },
        { title: 'China', value: 'china' },

    ],
}

export default zh_cn;