const en_us = {
    // global
    success: 'Succeed',
    no: 'NO',
    yes: 'YES',
    confirm: 'Confirm',
    cancel: 'Cancel',
    unexpected_error: 'Unexpected Error',
    network_error: 'Network Error',

    // login page
    email: 'Email',
    password: 'Password',
    login: 'Login',
    sign_up: 'Sign Up',

    // sign up page 
    first_name: 'First Name',
    last_name: 'Last Name',
    c_password: 'Confirm Password',
    address: 'Address',
    phone: 'Phone',
    login_picker_hint: 'Select The Date',
    created_at: 'Company Created At',

    // drawer 
    _profile: 'PROFILE',
    _account: 'ACCOUNTING',
    _setting: 'SETTING',
    _login: 'LOGIN',
    _logout: 'LOGOUT',
    _register: 'REGISTER',
    _login_msg: 'LOGIN TO SEE MORE DETAILS',
    _home: 'HOME',
    _event: 'EVENT',
    _notification: 'NOTIFICATION',
    _user: 'USER',
    _shop: 'SHOP',

    // header
    home: 'Home',
    event: 'Event',
    notification: 'Notification',
    setting: 'Setting',
    profile: 'Profile',
    terms: 'Terms and Policies',
    feedabck: 'Feedabck',
    contact: 'Contact Us',
    user: 'User',
    point_record: 'Point Record',
    event_record: 'Event Record',
    shop: 'Shop',
    feedback: 'Feedback',

    // home
    new: 'News',
    information: 'Information',
    view: 'View',
    box_1: 'POINT RECORD',
    box_2: 'EVENT RECORD',
    box_3: 'CREATE EXPENSE',
    box_4: 'CLAIM LIST',


    //setting
    s_language: 'Language',
    s_push: "Push Notifications",
    s_location: 'Location',
    s_currency: 'Currency',
    s_aboutAs: "About US",
    s_terms: "Terms and Policies",
    s_share: "Share our App",
    s_rate: 'Rate Us',
    s_feedback: "Send FeedBack",
    s_cache: 'Clear Cache',

    // picker
    change_currency_content: "Do you really want to change the currency from {{origin}} to {{new}}?",
    change_currency_success: "Change default currency succeed",
    change_location_content: "Do you really want to change the location from {{origin}} to {{new}}?",
    change_location_success: "Change default location succeed",
    change_language_content: 'Do you really want to change the language from {{origin}} to {{new}}?',
    change_language_success: 'Please restart the program to change the settings.',

    // accounting
    expenses: 'Expenses',
    expenses_des: 'List of your expenses',
    claims: 'Expenses Claims',
    claims_des: 'Review and submit your reports',
    expense: 'Expenses',
    createExpense: 'Create Expense',
    title: 'Title',
    amount: 'Amount',
    location: 'location',
    expenseType: 'Expense Type',
    currency: 'Currency',
    createExpense_picker_hint: 'Select Date',
    remarks: 'Remarks',
    accounting: 'Accounting',
    show: 'Show',
    edit: 'Edit',
    create: 'Create',
    claim: 'Claim',
    update: 'Update',
    confirm_delete: 'Do you really want to delete this record?',
    confirm_claim: 'Do you really want to claim this expense?',

    //profile
    points_record: 'Points Record',
    events_record: 'Events Record',
    approve: 'Approve',
    disapproved: 'Disapproved',
    ban: 'Ban',
    unban: 'Unbanned',

    //event
    join: 'Join',

    // shop
    confirm_exchange: 'Are you sure you want to exchange this item?',

    // currency
    usd: 'USD',
    hkd: 'HKD',
    rmb: 'RMB',
    eur: 'EUR',
    gbp: 'GBP',

    // location
    hk: 'Hong Kong',
    usa: 'USA',
    japan: 'Japan',
    china: 'China',

    // language
    zh_hk: "Traditional Chinese",
    zh_cn: "Simplified Chinese",
    en_us: "English",

    language_picker_type: [
        { title: "Traditional Chinese", value: 'zh_hk' },
        { title: "Simplified Chinese", value: 'zh_cn' },
        { title: "English", value: 'en_us' },
    ],

    currency_picker_type: [
        { title: 'HKD', value: 'hkd' },
        { title: 'RMB', value: 'rmb' },
        { title: 'USD', value: 'usd' },
        { title: 'EUR', value: 'eur' },
        { title: 'GBP', value: 'gbp' },
    ],

    location_picker_type: [
        { title: 'Hong Kong', value: 'hk' },
        { title: 'China', value: 'china' },
        { title: 'USA', value: 'usa' },
        { title: 'Japan', value: 'japan' },

    ],
}

export default en_us;